\chapter{Variables aleatòries discretes}

\section{Definició i objectes relacionats}

\begin{defi}[variable aleatòria!discreta]
    Sigui $\lp \Omega, \Asuc, p\rp$ un espai de probabilitat i sigui $X\colon \Omega\to\real$ una variable aleatòria. Diem que $X$ és discreta si
    $\im(X)$ és numerable.
\end{defi}

\begin{obs}
    En la pràctica, $\im(X) = \lc x_1 < x_2 < \dots \rc$ és un conjunt numerable ordenat. En els casos que veurem, $\im(X)\subseteq\z$.
    Escriurem $p_i = p(X=x_i)$.
\end{obs}

\begin{obs}
    Sigui $X$ una variable aleatòria discreta i sigui $A\in\mathcal{B}$. Aleshores,
    \[
        P_X(A) = p(X\in A) = \sum_{x_i\in A\cap\im\lp X\rp}{p_i}.
    \]
    %TODO Potser falta una X
\end{obs}

\begin{obs}
    Donat $\lc x_i \rc_{i \ge 1} \subset\real$ creixent i valors $\lc p_i \rc_{i \ge 1} \subset [0,1] \tq \sum{p_i}=1$, es pot definir
    una variable discreta $X$ que pren valors a $\lc x_i \rc$ tal que $p(X=x_i) = p_i$ $\forall\,i$.
\end{obs}

\begin{obs}
    La funció de distribució, amb $|\im(X)|<+\infty$, és
    \[F_X(x) = \begin{cases}
        0 &\text{si } x<x_1\\
        p_1+\cdots+p_j &\text{si } x_j \le x < x_{j+1}\\
        1 &\text{si } x \ge x_n
    \end{cases}.\]
\end{obs}

\begin{prop}[Operador esperança] Sigui $X$ una variable aleatòria discreta. Aleshores,
    \begin{enumerate}[i)]
        \item $\esp[X] = \sum\limits_{i \ge 1}{x_i p_i}$,
        \item Si $g\colon \real\to\real$ és una funció mesurable, $\esp[g(x)] = \sum\limits_{i \ge 1}{g(x_i) p_i}$.
    \end{enumerate}
\end{prop}

\begin{proof}
    \begin{enumerate}[i)]
        \item[]
        \item Fent servir la definició d'esperança tenim que
	  \[ \esp[X] = \int_{\real}{x\dif P_X} = \sum{x_i P_X(X=x_i) + \int_{\real\setminus\{x_i\}_{i\geq 1}}{x\mathbb{I}(x) \dif P_X}} = \sum_{i \ge 1}{x_i p_i} + 0\]
            perquè $P_X(\real\setminus\{x_i\}_{i\geq 1}) = 0$.
        \item Directe a partir del cas anterior i del fet que $g$ és mesurable.
    \end{enumerate}
\end{proof}

\begin{obs}
    Sigui $X$ una variable aleatòria. Aleshores,
    \[\var[X] = \esp[X^2] - \esp[X]^2 = \sum_{i \ge 1}{x_i^2 p_i} - \left(\sum_{i \ge 1}{x_i p_i}\right)^2.\]
\end{obs}

\begin{prop}
    Siguin $X, Y$ variables aleatòries discretes, amb $\im(X) = \lc x_i \rc_{i \ge 1}$ i $\im(Y) = \lc y_i \rc_{i \ge 1}$, i sigui $g\colon \real^2\to\real$ mesurable. Aleshores
    \[\esp[g(X,Y)] = \sum_{i,j \ge 1}{g(x_i,y_i) p(X=x_i, Y=y_i)}.\]
\end{prop}
\begin{proof}
    Directe a partir de la definició i del fet que $g$ és mesurable.
\end{proof}

\begin{prop}
    Siguin $X,Y$ variables aleatòries discretes. Són independents si i només si $\forall x \in\im(X)$ i $\forall y \in\im(Y)$,
    \[p(X=x, Y=y) = p(X=x)p(Y=y)\]
\end{prop}
\begin{proof}
    Exercici.
\end{proof}


\begin{defi}[vector de variables aleatòries!discret]
    Sigui $(X,Y)\colon \Omega\to\real^2$ un vector de variables aleatòries. Direm que és discret si $\im((X,Y))$ és numerable.
\end{defi}

\begin{obs}
    Sigui $(X,Y)$ vector de variables aleatòries. Aleshores és discret si i només si $X$ i $Y$ són discretes.
\end{obs}

\begin{defi}
    Sigui $(X,Y)$ vector de variables aleatòries discret. Definim
    \begin{align*}
        P_{\lp X,Y\rp}\colon\real ^2&\to\real\\
        \lp x, y\rp &\mapsto P_{\lp X,Y\rp}\lp x, y\rp \equiv p\lp X=x\rp p\lp Y=y\rp.
    \end{align*}
    Si $X,Y$ són independents, $P_{\lp X,Y\rp}\lp x, y\rp=p(X=x,Y=y)$.
\end{defi}

\begin{lema}
    Si $X,Y$ són variables aleatòries discretes independents amb $\esp[|X|] < +\infty$ i $\esp[|Y|] < +\infty$,
    \[\esp[XY] = \esp[X]\esp[Y].\]
\end{lema}

\begin{proof}
    \begin{align*}
        \esp[XY] &= \sum_{a\in\im\lp X\rp\cup\im\lp Y\rp} ap\lp XY=a\rp=\\
        &=\sum_{a\in\im\lp X\rp\cup\im\lp Y\rp} a\sum_{b\in\im\lp X\rp\setminus\lc 0\rc} p\lp X=b, Y=\frac{a}{b}\rp=\\
        &=\sum_{a\in\im\lp X\rp\cup\im\lp Y\rp} a\sum_{b\in\im\lp X\rp\setminus\lc 0\rc} p\lp X=b\rp p\lp Y=\frac{a}{b}\rp=\\
        &=\sum_{b\in\im\lp X\rp\setminus\lc 0\rc}\sum_{a\in\im\lp X\rp\cup\im\lp Y\rp} a p\lp Y=\frac{a}{b}\rp p\lp X=b\rp=\\
        &=\sum_{b\in\im\lp X\rp\setminus\lc 0\rc} bp\lp X=b\rp\sum_{a\in\im\lp X\rp\cup\im\lp Y\rp} \frac{a}{b} p\lp Y=\frac{a}{b}\rp=\\
        &=\sum_{b\in\im\lp X\rp\setminus\lc 0\rc} bp\lp X=b\rp\esp\lb Y\rb=\\
        &=\esp\lb X\rb\esp\lb Y\rb.
    \end{align*}
\end{proof}


\section{Funció generadora de probabilitat}
D'aquí en endavant, prendrem $X$ variable aleatòria discreta amb $\im(X) \subseteq \n_{\geq 0}$.
\begin{defi}[funció!generadora!de probabilitat]
    Definim la funció generadora de probabilitat d'$X$ com la sèrie formal de potències
    \[G_X(z) = \sum_{n \geq 0} p(X = n) z^n.\]
    Podem pensar-la també com $\esp[z^X]$.
\end{defi}

\begin{prop}
    $G_X(z)$ satisfà les següents propietats:
    \begin{enumerate}[i)]
    \item $G_X(z)$ \'es una funció holomorfa al voltant de $z = 0$ amb radi de convergència
        major o igual a $1$.
    \item $G_X(0) = p(X = 0)$ i $G_X(1)$ = 1.
    \item $\eval{\frac{\text{d}^kG_X(z)}{\text{d}z}}_{z = 1} =
        \esp\lb X(X-1)\dots(X-k+1)\rb = \esp \lb (X)_k\rb $.
    \end{enumerate}
\end{prop}

\begin{proof}
    \begin{enumerate}[i)]
        \item[]
        \item Si $\rho \in \cx$, $|\rho| < 1$, aleshores:
            \[
                0 \leq |G_X(\rho)| = \left|\sum_{n \geq 0} p(X = n) \rho^n \right|
                \leq \sum p(X=n) |\rho|^n
            \]
            que, quan $|\rho| \leq 1$, \'es menor o igual a
            \[
                \sum_{n \geq 0} p(X = n) = 1.
            \]
            Per tant $G_X(\rho)$ \'es analítica (es pot expressar com una sèrie de
            potències convergent) a $B_1(0)$, i per tant, com s'ha vist a variable
            complexa, $G_X(\rho)$ \'es holomorfa a $B_1(0)$ (per tant infinitament
            derivable en sentit complex).
        \item  Directe a partir de la definició.
        \item Si derivem terme a terme obtenim
            \[
                \frac{\text{d}^kG_X(z)}{\text{d}z} = \frac{\text{d}^k}{\text{d}z} \lp \sum_{n \geq 0} p(X = n) z^n\rp=
                \sum_{n \geq 0} n(n-1)\dots(n-k+1)p(X = n) z^{n-k},
            \]
            que avaluat en $z = 1$ \'es
            \[
                \sum_{n \geq 0} n(n-1)\dots(n-k+1)p(X=n) = \esp\lb(X)_n\rb.
            \]
    \end{enumerate}
\end{proof}

\begin{example}
   Definim $X$ com una variable aleatòria discreta tal que $p(X = 0) = 0$ i
   $p(X = n) = \frac{6}{\pi^2}\cdot \frac{1}{n^2}$. Aleshores
    \[
        G_X(z) =  \frac{6}{\pi^2} \sum_{n \geq 1} \frac{1}{n^2}z^n,
    \]
    que t\'e radi de convergència igual a $1$, i
    \[
        G_X(1) = \frac{6}{\pi^2} \sum_{n \geq 1} \frac{1}{n^2} =
        \frac{6}{\pi^2}\cdot \frac{\pi^2}{6} = 1.
    \]
    Finalment, en calculem la seva esperança,
    \[
        \esp[X] = \eval{\dv{G_X(z)}{z}}_{z = 1} =
        \frac{6}{\pi^2}\sum_{n \geq 1} \frac{1}{n} = \infty.
    \]
\end{example}

\begin{obs}
    $G_X(z)$ codifica totes les probabilitats $p(X = n)$ i per tant coneixent
    $G_X(z)$ coneixem $X$.
\end{obs}

L'aplicació m\'es útil de les funcions generadores de probabilitat \'es que
ens permet trobar convolucions discretes de variables aleatòries.

\begin{obs}
    Siguin $X, Y$ variables aleatòries discretes, amb $\im(X) = \im(Y) =
    \n_{\geq 0}$. Aleshores
    \[
        p(X+Y = n) = \sum_{k = 0}^{n} p(X+Y = n, X = k) =
        \sum_{k = 0}^{n} p(Y = n-k, X = k).
    \]
\end{obs}

\begin{prop}
    Si $X, Y$ són variables aleatòries discretes independents amb $\im(X) = \im(Y) =
    \n_{\geq 0}$ aleshores:
    \[
        G_{X+Y}(z) = G_X(z)G_Y(z)
    \]
\end{prop}

\begin{proof}
    \[
        G_X(z)G_Y(z) = \sum_{i \geq 0} p(X = i) z^i  \sum_{j \geq 0} p(X = j) z^j =
         \sum_{i, j \geq 0} p(X = i)p(Y = j) z^{(i+j)}.
    \]
    I, com $X$ i $Y$ són independents, podem unir el producte i obtenim
    \begin{align*}
        \sum_{i, j \geq 0} p(X = i, Y = j) z^{(i+j)} & = \sum_{n \geq 0} \sum_{i = 0}^n
        p(X = i, Y = n-i)z^n =\\
        &= \sum_{n \geq 0} \sum_{i = 0}^n p(X = i, X+Y = n)z^n =\\
        & = \sum_{n \geq 0} \sum_{i = 0}^n p(X = i | X+Y = n) p(X+Y = n)z^n = \\
        & = \sum_{n \geq 0} \ p(X+Y = n)z^n \sum_{i = 0}^n p(X = i | X+Y = n).
    \end{align*}
    Si observem que la suma interior val $1$ perquè està sumant la probabilitat de tots
    els esdeveniments possibles, ens queda
    \[
        \sum_{n \geq 0} p(X+Y = n)z^n = G_{X+Y}(z).
    \]
\end{proof}

\begin{obs}
    Això \'es equivalent a que si $X$ i $Y$ són variables aleatòries discretes independents
    amb $\im(X) = \im(Y) = \n_{\geq 0}$ aleshores
    \[
        \esp[z^X]\esp[z^Y] = \esp[z^{X+Y}].
    \]
\end{obs}

\begin{obs}
    En general, si $X_1, \dots, X_n$ són variables aleatòries discretes independents amb
    $\im X_i = \n_{\geq 0}$:
    \[
        G_{X_1+\cdots+X_n}(z) = \prod_{i = 1}^n G_{X_i}(z).
    \]
\end{obs}

\section{Models de variables aleatòries discretes}

En aquesta secció introduirem les variables aleatòries discretes més comunes que trobarem
% En aquesta secció hi ha demostracions sense tots els apartats. Això és intencionat, les que queden
% no s'han fet a classe perquè són trivials.

\begin{obs}
    En general escriurem $X \sim Y$ si $X$ i $Y$ tenen la mateixa distribució de probabilitat.
\end{obs}

\subsection*{Distribució de Bernoulli}

Modela l'èxit o fracàs d'un experiment amb probabilitat $p$ d'èxit.

\begin{defi}[distribució!de Bernoulli]
    Sigui $X$ una variable aleatòria. Direm que $X$ segueix una distribució de Bernoulli
    \[X \sim \operatorname{B}(p) \iff \begin{cases}
                       p(X=1) = p \\
                       p(X=0) = 1-p
                       \end{cases}.
    \]
    També es pot escriure $\operatorname{Be}(p)$.
\end{defi}

\begin{prop}
    Sigui $X$ una variable aleatòria que segueix una distribució de Bernoulli. Aleshores,
    \begin{enumerate}[i)]
        \item $G_X(z) = (1-p)z^0 + pz^1 = (1-p) + pz$
        \item $\esp[X] = p$
        \item $\var[X] = p(1-p)$
    \end{enumerate}
\end{prop}

\begin{proof}
    \begin{enumerate}[i)]
        \item[]
        \item[iii)] $\esp[x^2-x]= \esp[x(x-1)] = 0 \implies \esp[x^2] = p \implies \var[x] = \esp[x^2] - \esp[x]^2
        = p(1-p)$
    \end{enumerate}
\end{proof}

\subsection*{Distribució binomial}

Modela el nombre d'èxits en fer $N$ experiments independents, on cadascun és $\operatorname{B}(p)$.

\begin{defi}[distribució!binomial]
  Sigui $X$ una variable aleatòria. Direm que $X$ segueix una distribució binomial
    \[X \sim \operatorname{Bin}(N,p) \iff X = X_1 + \cdots + X_N,\]
    on $\lc X_i \rc_{i=1}^{N}$ són independents i $X_i \sim \operatorname{B}(p) \enspace\forall i$.
\end{defi}

\begin{prop}
    Sigui $X$ una variable aleatòria que segueix una distribució binomial. Aleshores,
    \begin{enumerate}[i)]
        \item $p(X=i) = \binom{N}{i}p^i(1-p)^{N-i}$
        \item $G_X(z) = \sum_{i=0}^{n} \binom{N}{i}p^i(1-p)^{N-i}z^i$
        \item $\esp[X] = N\esp[X_1] = Np$
        \item $\var[X] = N\var[X_1] = Np(1-p)$
    \end{enumerate}
\end{prop}

\begin{proof}
    \begin{enumerate}[i)]
        \item[]
        \item[ii)] Per ser $X_i$ independents,
        \begin{gather*}
        G_X(z) = G_{X_1 + \cdots + X_N}(z) = \prod_{i=1}^{n}G_{X_i}(z) = (pz+(1-p))^N = \\
        = \sum_{i=0}^n \binom{N}{i}(pz)^i(1-p)^{N-i} = \sum_{i=0}^{n} \binom{N}{i}p^i(1-p)^{N-i}z^i.
        \end{gather*}
    \end{enumerate}
\end{proof}

\begin{obs} La suma de dues variables independents amb aquesta distribució també té distribució binomial:
    \begin{gather*}
    \begin{rcases*} X \sim \operatorname{Bin}(N_1,p) \\ Y \sim \operatorname{Bin}(N_2,p) \end{rcases*} \implies
    \begin{rcases*} G_X(z) = (pz+(1-p))^{N_1} \\ G_Y(z) = (pz+(1-p))^{N_2} \end{rcases*}
    \implies \\
    \implies G_{X+Y}(z) = G_X(z)G_Y(z) = (pz+(1-p))^{N_1+N_2} \implies \\
    \implies X+Y \sim \operatorname{Bin}(N_1+N_2,p).
    \end{gather*}
\end{obs}

\subsection*{Distribució uniforme}

\begin{defi}[distribució!uniforme!discreta]
    Sigui $X$ una variable aleatòria. Direm que $X$ segueix una distribució uniforme
    \[X \sim \operatorname{U}[1,N] \iff p(X=i)=\frac{1}{N} \text{ per } i = 1,\dots,N.\]
\end{defi}

\begin{prop}
    Sigui $X$ una variable aleatòria que segueix una distribució uniforme. Aleshores,
    \begin{enumerate}[i)]
     \item $G_X(z) =  \frac{1}{N}\frac{z(z^N-1)}{z-1}$
     \item $\esp[X]=\frac{N+1}{2}$
     \item $\var[X]=\frac{N^2-1}{12}$
    \end{enumerate}
\end{prop}

\begin{proof}
    Directament de la definició de distribució uniforme tenim que
    \begin{enumerate}[i)]
        \item $G_X(z) = \sum_{n=1}^N \frac{1}{N}z^n = \frac{1}{N}(z+z^2+\cdots+z^N) = \frac{1}{N}\frac{z(z^N-1)}{z-1}$.
        \item $\esp[X]=\sum_{i=0}^N i\frac{1}{N}=\frac{1}{N}\frac{N\lp N+1\rp}{2}=\frac{N+1}{2}$.
        \item Tenim que $\var\lb X\rb = \esp\lb X^2\rb - \esp\lb X\rb ^2$, i fent servir l'apartat anterior  ens queda que $\var\lb X\rb = \lp\sum_{i=0}^N i^2\frac{1}{N}\rp-\lp\frac{N+1}{2}\rp ^2$. Fent servir la suma de quadrats i desenvolupant una mica finalment queda que
        \[
            \var\lb X\rb = \frac{4N^2+6N+2-\lp 3N^2+6N+3\rp}{12} =\frac{N^2-1}{12}.
        \]
  \end{enumerate}
\end{proof}


\subsection*{Distribució de Poisson}

S'usa per modelar successos ``estranys'' (persones en una cua, emissió de partícules, etc).

\begin{defi}[distribució!de Poisson]
    Sigui $X$ una variable aleatòria. Direm que $X$ segueix una distribució de Poisson
    \[X \sim \operatorname{P}(\lambda) \iff p(X=i) = \frac{\lambda^i }{i!}e^{-\lambda}.\]
    També es pot escriure $\operatorname{Po}(\lambda)$.
\end{defi}

\begin{obs}
    La distribució està ben definida:
    \[\sum_{i=0}^\infty p(X=i) = \sum_{i=0}^\infty \frac{1}{i!}\lambda^i e^{-\lambda} = e^\lambda e^{-\lambda} = 1.\]
\end{obs}

\begin{prop}
    Sigui $X$ una variable aleatòria que segueix una distribució de Poisson. Aleshores,
    \begin{enumerate}[i)]
        \item $G_X(z) = e^{\lambda(z-1)}$
        \item $\esp[X] = \lambda$
        \item $\var[X] = \lambda$
    \end{enumerate}
\end{prop}

\begin{proof}
    \begin{enumerate}[i)]
        \item[]
        \item $G_X(z) = \sum\limits_{i=0}^\infty \frac{1}{i!}\lambda^i e^{-\lambda} z^i =
            e^{-\lambda} \sum\limits_{i=0}^\infty \frac{1}{i!}(\lambda z)^i = e^{-\lambda}e^{\lambda z} = e^{\lambda(z-1)}$.
        \item $\esp[X] = \lambda e^{\lambda(z-1)}|_{z=1} = \lambda$.
        \item $\esp[X(X-1)] = \lambda^2 e^{\lambda(z-1)}|_{z=1} = \lambda^2 \implies \esp[X^2] = \lambda^2+\lambda$,  i per tant,
         $\var[X] = \esp[X^2] - \esp[X]^2 = \lambda^2 + \lambda - \lambda^2 = \lambda$.
    \end{enumerate}
\end{proof}

\begin{obs} La suma de dues variables independents amb distribució de Poisson també té distribució de Poisson:
    \begin{gather*}
    \begin{rcases*} X \sim \operatorname{Po}(\lambda_1) \\ Y \sim \operatorname{Po}(\lambda_2) \end{rcases*} \implies
    \begin{rcases*} G_X(z) = e^{\lambda_1(z-1)} \\ G_Y(z) = e^{\lambda_2(z-1)} \end{rcases*}
    \implies \\
    \implies G_{X+Y}(z) = G_X(z)G_Y(z) = e^{(\lambda_1+\lambda_2)(z-1)} \implies \\
    \implies X+Y \sim \operatorname{Po}(\lambda_1+\lambda_2).
    \end{gather*}
\end{obs}

\subsection*{Distribució geomètrica}
La distribució geomètrica representa el nombre d'experiments necessaris abans d'obtenir el primer èxit en un succés binari.
\begin{defi}[distribució!geomètrica]
    Sigui $X$ una variable aleatòria. Direm que $X$ segueix una distribució geomètrica
    \[X \sim \operatorname{Geom}(p) \iff p(X=i) = \lp 1-p \rp ^{i-1}p,\;\forall i\geq1.\]
\end{defi}

\begin{obs}
    La distribució està ben definida, atès que
    \[
        \sum_{i\geq 1} p\lp1-p\rp^{i-1} = p\sum_{i\geq 1}\lp1-p\rp^{i-1} = p\frac{1}{1-\lp 1-p\rp}=1.
    \]
\end{obs}

\begin{prop}
    Sigui $X$ una variable aleatòria que segueix una distribució geomètrica. Aleshores,
    \begin{enumerate}[i)]
        \item $G_X(z) =  \frac{pz}{1-\lp 1-p\rp z}$
        \item $\esp[X] = \frac{1}{p}$
        \item $\var[X] = \frac{1-p}{p^2}$
    \end{enumerate}
\end{prop}
\begin{proof}
    Directament de les definicions tenim que
    \begin{enumerate}[i)]
        \item $G_X(z) = \sum_{i\geq 1}\lp 1-p \rp ^{i-1}p z^i = \frac{p}{1-p}\sum_{i\geq 1}\lp\lp 1-p \rp z\rp^{i} =
        \frac{p}{1-p}\cdot\frac{\lp 1-p \rp z}{1-\lp 1-p\rp z}= \frac{pz}{1-\lp 1-p\rp z}$.
        \item
            \begin{align*}
                \esp\lb X\rb &= \lp \frac{\dif G_X\lp X\rp}{\dif z}\rp \lp 1\rp =\\
                &=\lp\frac{p\lp 1-\lp 1-p\rp z\rp+p\lp 1-p\rp z}{\lp 1-\lp 1-p\rp z\rp ^2}\rp\lp 1\rp =\\
                &=\frac{p^2+p-p^2}{p^2}=\\
                &=\frac{1}{p}.
            \end{align*}
        \item Es pot fer igual que en la distribució anterior: calculant $\esp\lb X\lp X-1\rp\rb$ amb la segona derivada de $G_X\lp z\rp$ i utilitzant que $\var\lb X\rb = \esp\lb X^2\rb - \esp\lb X\rb ^2$, però les expressions són farragoses i no ho farem.
    \end{enumerate}
\end{proof}

\subsection*{Distribució binomial negativa}

Modela el nombre d'experiments necessaris per aconseguir un nombre d'èxits $r$ donat.

\begin{defi}[distribució!binomial negativa]
    Sigui $X$ una variable aleatòria. Direm que $X$ segueix una distribució binomial negativa
    \[X \sim \operatorname{BinN}(r,p) \iff X = X_1 + \cdots + X_r,\]
    on $\lc X_i \rc_{i=1}^{r}$ són independents i $X_i \sim \operatorname{Geom}(p) \enspace\forall i$.
\end{defi}

\begin{obs}
    Una distribució binomial negativa és equivalent a $r$ distribucions geomètriques seguides.
\end{obs}


\begin{prop}
    Sigui $X$ una variable aleatòria que segueix una distribució binomial negativa. Aleshores,
    \begin{enumerate}[i)]
        \item $G_X(z) = \lp \frac{pz}{1-\lp 1-p\rp z}\rp^r$.
        \item $p(X=k) = \begin{cases}
                         0 &\text{ si } k < r\\
                         \binom{k-1}{r-1}p^{r}(1-p)^{k-r} & \text{ si }k\geq r
                        \end{cases}$
        \item $\esp[X] = r\esp[X_1] = \frac{r}{p}$.
        \item $\var[X] = r\var[X_1] = r\frac{1-p}{p^2}$.
    \end{enumerate}
\end{prop}

\begin{proof}
    \begin{enumerate}[i)]
        \item[]
        \item $G_X(z) = (G_{X_1}(z))^r = \lp \frac{pz}{1-\lp 1-p\rp z}\rp^r$.
        \item La probabilitat que es necessitin exactament $k$ experiments per obtenir $r$ èxits és la probabilitat d'obtenir $r-1$ èxits i $k-r$ fracassos als primers $k-1$ experiments per la probabilitat d'obtenir èxit al $k$-èssim experiment. Si $k<r$, això val $0$, com és natural, i si $k\geq r$, només cal comptar el nombre total de combinacions que donen lloc a aquest esdeveniment:
            \[
                p(X=k) = \lp\binom{k-1}{r-1}p^{r-1}(1-p)^{k-r}\rp p=\binom{k-1}{r-1}p^{r}(1-p)^{k-r}.
            \]
        \item
            \begin{align*}
                \esp\lb X\rb &= \lp \frac{\dif \lp G_X\lp X_1\rp\rp^r}{\dif z}\rp \lp 1\rp =\\
                &=\esp\lb X_1\rb \lp r\lp \frac{pz}{1-\lp 1-p\rp z}\rp ^{r-1} \rp\lp 1\rp =\\
                &=r\esp\lb X_1\rb.
            \end{align*}
        \item Es resol de forma anàloga als casos anteriors.
    \end{enumerate}
\end{proof}

\section{Distribucions condicionades i esperança condicionada}

\begin{defi}[funció!de distribució!condicionada]\idx{funció!de probabilitat!condicionada}
Siguin $X$ i $Y$ variables aleatòries discretes i sigui $x\in\real$ tal que $p\lp X=x\rp >0$. Aleshores definim
    \begin{enumerate}[1)]
        \item $F_{Y\vert X} \lp y,x\rp=p\lp Y\leq y\mid X=x \rp$ com la funció de distribució condicionada de $Y$ amb $X=x$,
        \item $P_{Y\vert X} \lp y,x\rp=p\lp Y = y\mid X=x \rp$ com la funció de probabilitat condicionada.
    \end{enumerate}
\end{defi}

\begin{obs}
 Una definició anàloga consisteix a prendre $A\in\Asuc$ enlloc de $X=x$ sempre que $p(A)>0$. Aleshores tenim
    \begin{enumerate}[1)]
        \item $F_{Y\vert A} \lp y\rp=p\lp Y\leq y\mid A \rp$
        \item $P_{Y\vert A} \lp y\rp=p\lp Y = y\mid A \rp$
    \end{enumerate}
\end{obs}

\begin{example}
    Siguin $\left\{Y_r\right\}_{r\geq 1}$ variables aleatòries independents tals que $Y_r \sim \operatorname{Be}(p)\; \forall i$. Sigui $X$ una
    variable aleatòria tal que $X=i$ si $Y_1 = Y_2 = \cdots = Y_{i-1}=0$ i $Y_i=1$. Observem que $X\mid\{Y_1=1\}= X\vert_{Y_1=1}=1$,
    $X\vert_{Y_1=0}=1+\operatorname{Geom}(p)$ i $X\vert_{Y_1=0,Y_2=0}=2+\operatorname{Geom}(p)$.
\end{example}

\begin{defi}[esperança!condicionada!discreta]
    Siguin $X,Y$ variables aleatòries discretes, i sigui $x$ tal que $p(X=x)>0$. L'esperança condicionada de $Y$ a $X=x$ és
    \[\esp[Y \vert X=x] = \sum_{y \in \im(Y)} y \cdot p(Y=y \vert X=x) = \phi(x).\]
    Observem que el valor de $\esp[Y \vert X=x]$ por variar depenent de l'elecció d'$x$.
\end{defi}

\begin{defi}[esperança!condicionada!discreta (2)]
    Siguin $X,Y$ variables aleatòries discretes i sigui $y\in\real$. Definim l'aplicació
    \begin{align*}
        \esp\lb Y\vert X\rb \colon \im\lp X\rp &\to \real \\
        x&\mapsto\esp\lb Y\vert X=x\rb=\phi\lp x\rp.
    \end{align*}
\end{defi}
\begin{obs}
    Observem que $\esp\lb Y\vert X\rb$ és una variable aleatòria i que
    \[
        p\lp\esp[Y \vert X]=y\rp = \sum_{x\in\phi^{-1}\lp y\rp}p\lp X=x\rp.
    \]
\end{obs}

\begin{example}
    \[\begin{rcases}
    p(X=1) = \frac{1}{4}, \phi(1)=2\\
    p(X=2) = \frac{1}{4}, \phi(2)=2
    \end{rcases} \implies p(\esp[Y \vert X]=2)=\frac{1}{4}+\frac{1}{4} = \frac{1}{2}.\]
\end{example}

\begin{obs}
    $\phi(x) = \esp[Y \vert X=x]$ és una funció sobre $x$ amb valors reals i, en canvi, $\esp[Y \vert X]$ és una variable aleatòria.
\end{obs}

\begin{prop}
    Sigun $X$ i $Y$ variables aleatòries discretes. Aleshores,
    \[\esp[\esp[Y \vert X]]= \esp[Y].\]
\end{prop}

\begin{proof}
    \begin{align*}
    \esp[\esp[Y \vert X]] = & \sum_{x\in\im(X)} \phi(x)p(X=x) = \\
    = & \sum_{x\in\im(X)} \sum_{y\in\im(Y)} y p(Y=y \vert X=x)p(X=x) = \\
    = & \sum_{y\in\im(Y)} \sum_{x\in\im(X)} y p(Y=y \vert X=x)p(X=x) = \\
    = & \sum_{y\in\im(Y)} y\sum_{x\in\im(X)} p(Y=y,X=x) = \\
    = & \sum_{y\in\im(Y)} y p(Y=y) = \esp[Y].
    \end{align*}
\end{proof}

\begin{obs}
    Siguin $X,Y$ variables aleatòries discretes independents. Aleshores, per qualsevol $ x \in\im(X)$,
    \[
        \esp[Y \vert X=x] = \sum_{y\in\im(Y)} yp(Y=y \vert X=x) = \sum_{y\in\im(Y)} yp(Y=y) = \esp[Y],
    \]
    i per tant $\esp[Y \vert X] = \esp[Y]$ amb probabilitat 1.
\end{obs}

\begin{obs}
    Per altra banda, si $Y=f(X)$ amb $f$ bijectiva,
    \[
        \phi(x) =  \sum_{y\in\im(Y)} yp(Y=y \vert X=x) = \sum_{z\in\im(X)} f(z)p(f(X)=f(z) \vert X=x) = f(x).
    \]
    i per tant $\esp[Y \vert X] = f(X) = Y$.
\end{obs}

\begin{example}
    $N \sim \operatorname{Po}(\lambda)$, $K \sim \operatorname{Bin}(N,p)$.
    Per cada $n$,
    \begin{align*}
        \phi\lp n\rp &= \esp[K \vert N=n] = \sum_{i\in\im(K)} ip(K=i \vert N=n) =\\
        &=\sum_{i=0}^n ip(K=i \vert N=n) = \sum_{i=0}^n i\binom{n}{i}p^i(1-p)^{n-i}=np.
    \end{align*}
    Per tant, $\esp[K \vert N] = np$ amb probabilitat $p(N=n) = \frac{1}{n!}\lambda^me^{-\lambda}$.

    \noindent Si volguéssim calcular $\esp[N \vert K]$, podríem prendre $k\in\im(K)$ i fer
    \begin{align*}
        \esp[N|K=k] &= \sum_{i\geq 0}ip(N=i \vert K=k) = \sum_{i\geq 0}i\frac{p(N=i \cap K=k)}{p(K=k)} =\\
        &=\sum_{i\geq 0}i\frac{p(K=k \vert N=i)}{p(K=k)}p(N=i).
    \end{align*}
\end{example}

\begin{obs}
    Com podem veure amb l'exemple anterior, si $\esp[Y \vert X=x] = \phi(x)$, aleshores $\esp[Y \vert X] = \phi(X)$.
\end{obs}

%TODO Exemple nombre de tirades fins que surt 6 sabent que ha sortit parell en un dau equilibrat

\section{Arbres de Galton-Watson}

Els arbres de Galton-Watson són una aplicació dels conceptes de distribució condicionada i esperança condicionada.

Sigui $X$ una variable aleatòria discreta amb $\im\lp X\rp=\n$. Considerem ara l'evolució de següent procés estocàstic. Sigui $Z_n$ el nombre d'individus de l'$n$-èssima generació i posem $Z_0$=1. Cada individu de la generació $n$ té fills seguint la distribució $X$, i els fills dels individus de la generació $n$ conformen la generació $n+1$. Volem estudiar $Z_n$ i determinar amb quina probabilitat la població s'extingeix en alguna generació.

\begin{lema}
    Sigui $\lc X_i\rc_{i\geq 1}$ una seqüència de variables aleatòries discretes amb imatge en $\n$, idènticament distribuïdes i amb funció generadora de probabilitat $G_X\lp z\rp$. Sigui $N$ una variable aleatòria discreta independent dels $X_i$ amb imatge en $\n$ i funció generadora de probabilitat $G_N\lp z\rp$. Sigui $S=X_1+\cdots+X_N$ una variable aleatòria discreta. Aleshores,
    \[
        G_S\lp z\rp = G_N\lp G_X\lp z\rp \rp.
    \]
\end{lema}
\begin{proof}
    \begin{align*}
        G_S\lp z\rp &= \esp\lb z^S\rb = \esp\lb\esp\lb z^S\vert N\rb\rb =\sum_{n\geq 1} \esp\lb z^{X_1+\cdots+X_n}\rb p\lp N=n\rp=\\
        &=\sum_{n\geq 1} \prod_{i=1}^{n} \esp\lb z^{X_i}\rb p\lp N=n\rp=\sum_{n\geq 1} \lp G_X\lp z\rp\rp^n p\lp N=n\rp=\\
        &=G_N\lp G_X\lp z\rp \rp.
    \end{align*}
\end{proof}

\begin{teo*}
    Sigui $G_n\lp z\rp$ la funció generadora de probabilitat de $Z_n$. Aleshores,
    \[
        G_{n+m}\lp z\rp = G_n\lp G_m\lp z\rp\rp
    \]
    i, per tant, $G_n\lp z\rp = \underbrace{G_X\circ\cdots\circ G_X\lp z\rp}_{n\text{ termes}}$.
\end{teo*}
\begin{proof}
    Primer observem que cada individu de la generació $n+m$ té exactament un ascendent en la generació $m$. Així doncs,
    \[
        Z_{n+m}=\sum_{i=1}^{Z_m} Y_i,
    \]
    on $Y_i$ és el nombre de membres de la generació $n+m$ amb ancestre $i$ a la geneació $m$. Aquestes $Y_i$ són variables aleatòries discretes independents i amb distribució igual a $Z_n$. En virtut del lema anterior, concloem el que volíem: $G_{n+m}\lp z\rp = G_n\lp G_m\lp z\rp\rp$.
\end{proof}

\begin{obs}
    En general, és difícil calcular $G_n\lp z\rp$, però, en canvi, $\esp\lb Z_n\rb$ i $\var\lb Z_n\rb$ són fàcils de calcular.
    \begin{align*}
        \esp\lb Z_n\rb &= \esp\lb X\rb ^n,\\
        \var\lb Z_n\rb &=
        \begin{cases}
            n\var\lb X\rb & \text{ si } \esp\lb X\rb=1,\\
            \var\lb X\rb\esp\lb X\rb^ {n-1}\frac{\esp\lb X\rb^n-1}{\esp\lb X\rb-1} & \text{ altrament}.
        \end{cases}
    \end{align*}
\end{obs}

Estudiem ara quan es produeix l'extinció de la població. Denotem $A_n=\lc Z_n=0\rc$, és a dir, els successos pels quals la població ja s'ha extingit a la generació $n$ i denotem també $\ex=\bigcup_{n\geq 1} A_n$, és a dir, els successos pels quals la població s'extingeix en algun moment. Observem que $A_1\subseteq A_2\subseteq\cdots$ i que, per tant, $p\lp\ex\rp=\lim_{n\to\infty}p\lp Z_n=0\rp$.

\begin{teo*}
    Sigui $\esp\lb X\rb=\mu$ i sigui $\eta$ la solució positiva més petita de l'equació del punt fix $G_X\lp z\rp = z$. Aleshores, $p\lp\ex\rp=\eta$. En particular,
    \begin{enumerate}[i)]
        \item $\mu<1\implies\eta=1$. La població s'extingeix amb probabilitat $1$ (règim subcrític).
        \item $\mu>1\implies\eta<1$. La població pot no extingir-se (règim supercrític).
        \item $\mu=1\implies\eta=1$, sempre que $\var\lb X\rb>0$. La població s'extingeix amb probabilitat $1$.
    \end{enumerate}
\end{teo*}

\begin{proof}
	Sigui $\eta_n=p\lp Z_n=0\rp=G_n\lp 0\rp = G_X\lp G_{n-1} \lp 0 \rp \rp= G_X\lp\eta_{n-1}\rp,\forall n>0$ i amb
    $\eta_0=0$. Aleshores, cal estudiar la solució límit de
    l'equació $\eta_n=G_X\lp\eta_{n-1}\rp$. Per fer-ho, estudiarem el comportament de
    la funció $G_x$ a l'interval $[0,1]$. Recordem que aquesta funció t\'e totes les
    derivades positives en aquest interval (en particular \'es creixent i convexa) i
    la seva derivada a $z = 1$ \'es $\mu = \esp[X]$. Farem un esquema de la prova dividint
    el problema en els tres casos corresponents a l'enunciat, \'es a dir, segons si
    $\mu < 1$, $\mu = 1$ o $\mu > 1$. En tots tres casos, observem que la iteració de
    $\eta_n$ convergeix al primer punt fix de la funció, i que $z = 1$ sempre \'es
    punt fix.

    \begin{enumerate}[i)]
        \item Quan $\mu < 1$, el primer punt fix serà a $z = 1$, ja que en cas contrari
            la convexitat de la funció junt amb el fet que $G_X(1) = 1$ obligaria a
            la derivada a ser m\'es gran que $1$ a $z = 1$.
        \item Quan $\mu = 1$, l'anàlisi \'es equivalent al cas anterior però cal afegir
            que si $\var[X] = 0$ aleshores $G_X(0) = 0$ cosa que trenca l'anàlisi anterior.
            En aquest cas, no hi ha extinció.
        \item Quan $\mu > 1$, $G_X$ \'es necessàriament menor que la recta $z$ a un entorn
            de $1$, i per tant hi ha un punt fix abans. Així doncs, l'extinció serà amb
            probabilitat $\eta$, amb $\eta$ primer punt fix de $G_x$.
    \end{enumerate}

\end{proof}
