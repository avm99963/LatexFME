\chapter{Teoria de grafs extremals}

L'objectiu principal de la teoria de grafs extremals és l'estudi dels anomenats grafs extremals: 
grafs maximals respecte el nombre d'arestes que no continguin cap còpia d'un cert graf $H$ com a subgraf. 

\section{Grafs sense triangles}

Comencem preguntant-nos quantes arestes pot tenir un graf de $n$ vèrtexs que no contingui
cap triangle. Un primer exemple seria $K_{\lfloor \frac{n}{2} \rfloor,\lceil \frac{n}{2} \rceil}$,
que té $\lfloor \frac{n}{2} \rfloor\lceil \frac{n}{2} \rceil$ arestes. Tot seguit, veurem 
que aquesta fita és òptima.

\begin{lema}
	Si $xy$ és una aresta en un graf de $n$ vèrtexs sense triangles, aleshores $d(x) + d(y) \leq n$.
\end{lema}
\begin{proof}
	Només cal observar que $x$ i $y$ no poden tenir cap veí en comú, ja que això causaria un triangle.
	Per tant, $(d(x) - 1) + (d(y) - 1) \leq n-2$, d'on es dedueix el resultat.
\end{proof}

\begin{teo}[Teorema de Mantel]
	Un graf $\Gamma$ de $n$ vèrtexs sense triangles té, com a molt, 
	$\lfloor \frac{n}{2} \rfloor\lceil \frac{n}{2} \rceil$ arestes.
\end{teo}
\begin{proof}
	Sigui $A$ un conjunt de vèrtexs independent de mida màxima (i.e. no hi ha cap aresta
	entre vèrtexs de $A$), amb $|A| = a$. Atès que $\Gamma$ no té cap triangle, els veïns d'un vèrtex $x$ donat
	formen un conjunt independent, i per tant $d(x) \leq a$ per a tot vèrtex $x$.
	
	Sigui $B = V(\Gamma) \setminus A$, amb $|B| = b$. Atès que tota aresta ha de ser incident
	a algun vèrtex de $B$, el nombre d'arestes $m$ satisfà:
	\[
		m \leq \sum_{x \in B} d(x) \leq ab \leq \lp \frac{a+b}{2} \rp^2 = \frac{n^2}{4},
	\]
	on a l'última igualtat hem usat la desigualtat entre la mitjana geomètrica i l'aritmètica.
	Se segueix el resultat, ja que $\lfloor \frac{n}{2} \rfloor\lceil \frac{n}{2} \rceil = \frac{n^2}{4}$
	si $n$ és parell i $\lfloor \frac{n}{2} \rfloor\lceil \frac{n}{2} \rceil = \frac{1}{4}(n^2 - 1)$ si $n$
	és imparell.
\end{proof}

Finalment, ens preguntem quins són tots els casos d'igualtat. Per al cas on $n$ és parell, la igualtat
requereix que totes les arestes connectin un vèrtex de $A$ i un de $B$,
$d(x) = a$ per a tot vèrtex $x$ de $B$, i que
\[
	ab = \lp \frac{a+b}{2} \rp^2 \implies a = b = \frac{n}{2},
\]
com es pot comprovar. Així, l'únic cas possible és $K_{\frac{n}{2}, \frac{n}{2}}$, que és el
que havíem trobat abans. Un argument anàleg ens porta a que $K_{\frac{n-1}{2}, \frac{n+1}{2}}$
és el cas òptim per a $n$ senar.

\section{Grafs sense subgrafs complets}

Tot seguit, generalitzem el resultat anterior per a grafs que no continguin cap subgraf complet $K_{r+1}$.
Si abans hem partit el graf en dues parts per evitar triangles, sembla raonable que ara partim el graf en
$r$ parts per evitar que aparegui $K_{r+1}$.

\begin{defi}[graf!multipartit]
	Un graf multipartit o $r$-partit és un graf $G$ els vèrtexs del qual es poden partir en 
	$r$ conjunts independents. Equivalentment, $G$ és $r$-partit si és $r$-colorejable.
\end{defi}

Pel principi del colomar, sempre que seleccionem $r+1$ vèrtexs d'un graf $r$-partit n'existiran dos que caiguin
a la mateixa part, i per tant no estaran connectats per una aresta. Si volem que un graf $r$-partit
tingui el màxim nombre d'arestes, haurem de repartir els vèrtexs de manera que les parts siguin tan iguals
com sigui possible (amb $\lfloor \frac{n}{r} \rfloor$ o $\lceil \frac{n}{r} \rceil$ vèrtexs segons el cas).
Aquest graf tindrà, aproximadament, $\frac{1}{2}n(n-\frac{n}{r}) = \frac{1}{2}n^2 (1 - \frac{1}{r})$ arestes.

\begin{defi}[graf!de Turán]
	Un graf de Turán és un graf $T_{n,r}$ de $n$ vèrtexs i $r$-partit amb el màxim nombre d'arestes.
	Denotarem $t_{n,r} = |E(T_{n,r})|$.
\end{defi}

\begin{teo}[Teorema de Turán]
	Un graf amb $n$ vèrtexs que no contingui $K_{r+1}$ té, com a molt, $t_{n,r}$ arestes.
\end{teo}
\begin{proof}
	Sigui $\Gamma$ un graf amb nombre maximal d'arestes que no contingui $K_{r+1}$. El pas clau serà 
	demostrar que, en $\Gamma$, la no-adjacència és una relació d'equivalència. Les propietats
	reflexiva i simètrica són immediates (i certes en general). Quant a la transitiva, suposem que
	$x$ no és adjacent a $y$, i que $y$ tampoc ho és a $z$, però que $xz \in E(\Gamma)$. Considerem
	dos casos:
	\begin{itemize}
		\item $d(y) < d(x)$ (o, simètricament, $d(y) < d(z)$). Construïm el graf $\Gamma'$, obtingut
		a partir de $\Gamma$ esborrant $y$ i afegint una còpia $x'$ de $x$ (i.e. $x'$ té les mateixes
		adjacències que $x$, i ambdós vèrtexs no estan connectats). El nou graf $\Gamma'$ no conté cap
		còpia de $K_{r+1}$, ja que aquesta còpia hauria de contenir tant $x$ com $x'$, però aquests dos vèrtexs
		no estan connectats. Però $\Gamma'$ té més arestes que $\Gamma$, contradient la maximalitat.
		
		\item $d(y) \geq d(x),d(z)$. Fem el mateix procediment que abans, però ara esborrant $x,z$ i
		afegint dues còpies $y',y''$. El graf obtingut $\Gamma'$ té, com a mínim, una aresta més que $\Gamma$,
		ja que $x$ i $z$ estaven connectats, i $\Gamma'$ no conté $K_{r+1}$. Altra vegada, contradicció.
	\end{itemize}
	Per tant, la no-adjacència és una relació d'equivalència. Per tant, podem dividir el graf en diferents parts,
	on cada part conté vèrtexs no adjacents entre ells. Com a molt, podem tenir $r$ parts: si en tinguéssim
	$r+1$, podríem agafar $r+1$ vèrtexs de cadascuna de les $r+1$ parts diferents, i tots estarien connectats entre sí.
	Per tant, hem de tenir com a molt $r$ parts, i com que tenir més parts ens permet tenir més arestes, podem
	considerar que $\Gamma$ és $r$-partit.
	
	Si volem que $\Gamma$ tingui el màxim nombre d'arestes, hem de distribuir els vèrtexs al més equitativament
	possible en les $r$ parts, i això s'aconsegueix amb un graf de Turán.
\end{proof}

\section{Teorema d'Erd\H{o}s-Stone}

Fins ara, ens hem restringit a estudiar alguns casos particulars de subgrafs prohibits. El Teorema d'Erd\H{o}s-Stone
dona una fita asimptòtica per a grafs $H$ generals.

\begin{defi}
	Donat un graf $H$, definim $ex(n,H)$ com el màxim nombre d'arestes que pot tenir un graf de $n$
	vèrtexs que no contingui $H$ com a subgraf.
\end{defi}

\begin{teo}[Teorema d'Erd\H{o}s-Stone]
	Per a tot $\varepsilon > 0$, existeix $n_0$ tal que per a tot $n \geq n_0$, se satisfà:
	\[
		\frac{1}{2} n^2 \lp 1- \frac{1}{\chi(H) - 1} - \varepsilon \rp \leq ex(n,H)
		\leq \frac{1}{2} n^2 \lp 1- \frac{1}{\chi(H) - 1} + \varepsilon \rp
	\]
\end{teo}
La demostració ve donada en una sèrie d'exercicis del curs, i no la farem aquí.
\begin{example}
	Si $H = K_{r+1}$, com hem fet abans, tenim que $\chi(H) -1 = r$ i, per tant, recuperem la fita anterior.
	Així, el teorema d'Erd\H{o}s-Stone generalitza el teorema de Turán.
\end{example}

Observem que si $\chi(H) \neq 2$, el teorema ens dona el comportament asimptòtic de $ex(n,H)$, que és
una constant coneguda multiplicada per $n^2$. En canvi, si $\chi(H) = 2$, el teorema pràcticament no ens dona informació
més enllà que la dependència serà menor que $n^2$. Així, en el cas bipartit encara hi ha feina a fer.

\section{Grafs sense subgrafs bipartits complets}

\begin{example}
	El cas més senzill a considerar és $H= K_{1,t}$, en el qual l'únic que s'ha d'evitar és que algun vèrtex tingui
	$t$ veïns. Per tant, si aconseguim que tots els vèrtexs tinguin grau $t-1$, hauríem de tenir
	$ex(n,K_{1,t}) \approx \frac{1}{2}n(t-1)$.
\end{example}

\begin{example}
	Considerem un altre cas petit (però més complicat), que és $H = K_{2,2} = C_4$.
	Construirem l'exemple insiprant-nos en geometries finites: Sigui $(P,L)$ un espai projectiu d'ordre $q$.
	Construïm un graf bipartit, on els vèrtexs de cada part són $P$ i $L$ respectivament, i posem una aresta
	entre $p$ i $l$ si, i només si, $p \in l$. Com que dos punts estan units per una sola recta, aquest graf no conté
	cap còpia de $K_{2,2}$. En aquest graf,
	\[
		\begin{rcases}
			n = 2(q^2 + q + 1) < 2(q+1)^2 \\
			m = \frac{1}{2} n(q+1)
		\end{rcases}
		\implies m > \lp \frac{1}{2} n\rp^{3/2}.
	\]
\end{example}

\begin{lema}
	Siguin $t,d_1,\dots,d_n$ enters no negatius. Aleshores:
	\[
		\sum_{i=1}^n \binom{d_i}{t} \geq n \binom{\lfloor \frac{1}{n} \sum_{i=1}^n d_i \rfloor}{t}.
	\]
\end{lema}
La demostració es deixa com a exercici. S'indica que la demostració es pot fer per inducció en $t$.

\begin{teo*}
	Per a tot $\varepsilon > 0$, existeix $n_0$ tal que per a tot $n \geq n_0$, tot graf amb $n$ vèrtexs que no
	contingui $H = K_{t,s}$ té, com a molt, $\frac{1}{2}(s-1)^{1/t} (1+ \varepsilon) n^{2-1/t}$ arestes.
\end{teo*}
\begin{proof}
	Sigui $\Gamma$ un graf amb $n$ vèrtexs, $m$ arestes i sense $K_{t,s}$. Sigui $N$ el nombre de còpies
	de $K_{1,t}$ contingudes en $\Gamma$. Un conjunt donat de $t$ vèrtexs pot tenir, com a molt,
	$s-1$ veïns en comú, ja que sinó es crearia una còpia de $K_{t,s}$. Vist d'una altra manera,
	aquests $t$ vèrtexs poden formar part de, com a molt, $s-1$ còpies de $K_{1,t}$. En conseqüència,
	$N \leq \binom{n}{t} (s-1)$.
	
	Observem que un vèrtex de grau $d(v)$ contribueix en $\binom{d(v)}{t}$ còpies diferents de $K_{1,t}$.
	Així, si $\delta = \frac{2m}{n}$ és el grau mitjà dels vèrtexs, pel lema es té:
	\[
		N = \sum_{v \in V(\Gamma)} \binom{d(v)}{t} \geq n \binom{\lfloor \delta \rfloor}{t}.
	\]
	Suposem, buscant una contradicció, que $m > \frac{1}{2} (s-1)^{1/t} (1+ \varepsilon) n^{2-1/t}$.
	Així, s'ha de tenir
	\[
		\lfloor \delta \rfloor > (s-1)^{1/t} (1+\varepsilon) n^{1-1/t} -1.
	\]
	Tenint en compte les dues desigualtats obtingudes anteriorment per $N$, i considerant únicament el
	primer terme en $n$ en els binomials, arribem a:
	\[
		(1+\varepsilon)^{t-1} \frac{n^t}{t!} (s-1) > N > \frac{n}{t}(s-1)(1+\varepsilon)^t n^{t-1} - Cn^{t-1+t/1},
	\]
	sent $C$ una constant que no ens interessa saber. Reagrupant termes,
	\[
		C n^{t-1+1/t} > \frac{n^t}{t!} (s-1)(1+\varepsilon)^{t-1} (1+\varepsilon-1),
	\]
	que és una contradicció per a $n$ suficientment gran.
\end{proof}

La fita que ens dona aquest teorema per $H = K_{2,2}$ és $ex(n,H) < \frac{1}{2}(1+\varepsilon) n^{3/2}$.
Abans, hem construït un graf amb aproximadament $\frac{1}{2\sqrt{2}} n^{3/2}$ arestes, que té l'exponent
que toca, però una constant que no és òptima. Tot seguit, farem una construcció que ens portarà a l'òptim.

\begin{defi}[polaritat]
	Donat un pla projectiu $\Pi = (P,L)$, una polaritat és una bijecció $\sigma \colon P \to L$ tal
	que $x \in \sigma(y) \iff y \in \sigma(x)$.
\end{defi}
Recordem que en $PG(2,q)$, els punts són $P = \{(x_1:x_2:x_3) | \, (x_1,x_2,x_3) \in \F_q^3\}$, 
on $(x_1:x_2:x_3)$ és el subespai 1-dimensional generat per $(x_1,x_2,x_3)$, i les rectes són de la forma
$a_1 X_1 + a_2 X_2 + a_3 X_3 = 0$, $a_i \in \F_q$. Definim la següent aplicació:
\[
	\sigma \colon (x_1:x_2:x_3) \mapsto \ker(x_1 X_1 + x_2 X_2 + x_3 X_3).
\]
L'aplicació és clarament bijectiva, i és una polaritat:
\[
	y \in \sigma(x) \iff  x_1 y_1 + x_2 y_2 + x_3 y_3 = 0 \iff x \in \sigma(y).
\]
Construïm ara un graf $\Gamma$ amb vèrtexs $P$, i sigui $\sigma$ una polaritat (per exemple, l'anterior).
A $\Gamma$, posem una aresta entre $x$ i $y$ si, i només si, $x \in \sigma(y)$ (i, per tant, $y \in \sigma(x)$).
Atès que $n = q^2 + q + 1$, i que cada punt és incident a $q+1$ rectes, el nombre d'arestes $m$ satisfà:
\[
	m > \frac{1}{2}nq \sim \frac{1}{2}n^{3/2}.
\]
Així, aquesta nova construcció assoleix l'òptim del teorema. No obstant, cal remarcar que el teorema no ens
garanteix que la fita sigui òptima: ens dona una fita superior, però no inferior. Tot seguit, farem
una construcció probabilística que ens donarà una fita inferior.

\begin{teo*}
	Per a una constant $c$ i una $n$ suficientment gran, existeix un graf de $n$ vèrtexs
	que no conté $K_{t,s}$ i té $cn^{2-\lp \frac{s+t-2}{st-1} \rp}$ arestes.
\end{teo*}
\begin{proof}
	Agafem un graf amb $n$ vèrtexs, de manera que posem una aresta entre dos vèrtexs qualssevol amb probabilitat $p$.
	Sigui $Y$ la variable aleatòria que compta el nombre d'arestes, i sigui $X$ la variable aleatòria que compta el
	nombre de còpies del subgraf $K_{t,s}$. Es té:
	\begin{align*}
		& \esp[Y] = \binom{n}{2}p > cn^2 p \\
		& \esp[X] = \binom{n}{t} \binom{n-t}{s}p^{st} < c' n^{s+t}p^{st}.
	\end{align*}
	Com que l'esperança és lineal, s'ha de tenir:
	\[
		\esp[Y-X] > cn^2p- c' n^{s+t}p^{st}.
	\]
	Tot seguit, escollim una $p$ per tal que el membre de la dreta doni com a resultat $\frac{1}{2}cn^2p$.
	Operant, arribem a que $p = c''n^{\frac{2-(s+t)}{st-1}}$. Aleshores,
	\[
		\esp[Y-X] > \frac{1}{2} c n^{2-\frac{s+t-2}{st-1}}.
	\]
	Per tant, existirà algun graf $\Gamma$ tal que $Y-X > \frac{1}{2} c n^{2-\frac{s+t-2}{st-1}}$.
	A $\Gamma$, eliminem una aresta de cada còpia de $K_{t,s}$ que hi hagi. Aquesta operació manté constant
	el valor de $Y-X$, i continua fins que $X = 0$. D'aquesta manera, ens quedarà un graf sense còpies de $K_{t,s}$ i amb
	$m > c n^{2-\frac{s+t-2}{st-1}}$ arestes.
\end{proof}

Aquesta fita ens dona, per exemple, $cn^{4/3}$ per $K_{2,2}$, que és pitjor del que ja sabíem. En canvi,
aquesta és la millor fita que es coneix pel cas $t \geq 6$, $s \leq (t-1)!$.

\section{Grafs sense cicles parells}

Finalment, ens preguntem pels grafs que no contenen cap còpia de $C_{2t}$. El cas de $C_4 = K_{2,2}$
ja el coneixem d'abans. Així doncs, ens preguntem pel següent cas, que és $C_6$. Farem una construcció 
basant-nos en l'espai projectiu de 3 dimensions. 

En primer lloc, considerem que tenim una polaritat $\sigma$, aquest cop definida dels punts als plans de $PG(3,q)$.
Assumirem, a més, que $x \in \sigma(x)$ per a tot punt $x$. Considerem una estructura d'incidència $(P,L)$ tal que 
$P$ són els punts de $PG(3,q)$ i $L$ són les rectes tals que per a tot $x \in L$, $L \subseteq \sigma(x)$. Comptant
parelles $(x,l)$ tals que $x \in l$, tenint en compte que $\sigma(x)$ és un pla projectiu, s'arriba a
\[
	|P|(q+1) = |L|(q+1) \implies |P| = |L|.
\]
A més, per \ref{teo:subespProj}, s'ha de tenir $|P| = |L| = \frac{q^4-1}{q-1} = q^3 + q^2 + q + 1$.
Hom pot comprovar que, per aquesta estructura d'incidència, per a tota parella $x,l$ tal que $x \notin l$,
existeix un únic punt $y \in l$ que està alineat amb $x$. Per tant, aquesta estructura d'incidència
no té triangles.

\begin{defi}
	Un quadrangle generalitzat és una estructura d'incidència amb la propietat que per a tot $(x,l) \in (P,L)$ tal que
	$x \notin l$, existeix un únic punt $y \in l$ alineat amb $x$.
\end{defi}

\begin{ej}
	Completeu els detalls que s'han deixat.
\end{ej}

Manca comprovar que existeix alguna polaritat tal que $x \in \sigma(x)$. Definim la següent aplicació:
\[
	\sigma(x_1:x_2:x_3:x_4) = \ker(x_1X_2-x_2X_1 + x_3X_4-x_4X_3).
\]
És fàcil comprovar que $\sigma$ és polaritat, i que $x \in \sigma(x)$. A la vista dels resultats,
construïm un graf bipartit $\Gamma$, amb parts $P$, $L$, de manera que posem una aresta si, i només si,
$x \in l$. Aquest graf no conté $C_6$, ja que un $C_6$ es correspondria amb un triangle a l'estructura
d'incidència original (comproveu-ho).

Finalment, observem que $n \approx 2q^3$, i $m\approx \frac{1}{2}nq \approx cn^{4/3}$. En general, tenim
la següent fita superior:
\begin{teo*}
	Un graf amb $n$ vèrtexs que no contingui $C_{2t}$ té, com a molt, $cn^{1+1/t}$ arestes,
	per a certa constant $c$.
\end{teo*}
Als problemes de l'assignatura, construirem amb el mètode probabilístic un graf amb $n$ vèrtexs i sense $C_{2t}$ 
que tindrà, com a mínim, $c'n^{1+1/(2t-1)}$ arestes.
