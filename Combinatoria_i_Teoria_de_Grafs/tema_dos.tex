\chapter{Enumeració simbòlica. Funcions generadores}
\section{Sèries formals de potències}

\begin{defi}[sèrie formal de potències]
	Donat el conjunt $\cx[[z]] =\cx^{\n} = \{ (c_0,c_1,c_2,\dots) \, | c_i \in \cx \}$ 
	de successions de complexos, es defineix l'anell de sèries formals de potències
	$(\cx[[z]],+,\cdot)$ amb les operacions següents: Si $a = (a_n \colon n \in \n)$ i
	$b = (b_n \colon n \in \n)$ són successions de complexos, es defineix:
	\begin{itemize}
		\item La seva suma: $a+b = (a_n + b_n \colon n\in \n)$.
		\item El seu producte: $a \cdot b = (\sum_{k=0}^{n} a_k b_{n-k} \colon n \in \n)$.
	\end{itemize}
	En aquest cas, la unitat pel producte és $1 = (1,0,\dots,0,\dots)$.
\end{defi}

Si fem la identificació $z = (0,1,0,0,\dots)$, aleshores tot $a \in \cx[[z]]$ es pot escriure
de la forma
\[
	a = \sum_{n \geq 0} a_n z^n,
\]
considerant $a_n = (a_n,0,0,\dots)$ i $z^n = (0,0,\dots,1,\dots)$, on l'1 de $z^n$ està en
la posició $n$-èsima. També es denota $[z^n] a = a_n$.

\begin{prop} \label{prop:InvSeries}
	$a \in \cx[[z]]$ és invertible (pel producte) si i només si $a_0 \neq 0$.
\end{prop}
\begin{proof}
	Si $a$ és invertible, aleshores existeix $b$ tal que $ab = 1$, de manera que
	$a_0 b_0 = 1$, i per tant $a_0 \neq 0$.
	
	Recíprocament, si $a_0 \neq 0$, volem construir $b = (b_n \colon n \in \n)$ de manera que
	$ab = 1$. Farem la construcció recursivament, terme a terme:
	\[
		\begin{aligned}
			a_0 b_0 = 1 & \implies & b_0 = a_0^{-1}, \\
			a_1b_0 + a_0b_1 = 0 & \implies & b_1 = a_0^{-1} (-a_1 b_0), \\
			 & \cdots & \\
			a_nb_0 + a_{n-1}b_1 + \dots + a_0 b_n = 0 & \implies & b_n = -a_0^{-1}(a_nb_0 + \dots + a_1b_{n-1}).
		\end{aligned}
	\]
\end{proof}

\begin{example}
	L'exemple més típic de sèries formals de potències és:
	\[
		\frac{1}{1-z} = 1 + z + z^2 + z^3 + \dots.
	\]
	Aquesta notació l'únic que vol dir és que l'invers de $1-z$ respecte el producte és
	$1+z+z^2+z^3+\dots$, com es pot comprovar amb facilitat. També es té:
	\[
		e^{z} = \sum_{n \geq 0} \frac{z^n}{n!},
	\]
	que des del punt de vista de sèries formals de potències només és una definició, amb la
	qual se satisfà $e^{z+w} = e^z e^w$.
\end{example}

\begin{example}
	Donada la sèrie
	\[
		\frac{1}{(1-z)^{m+1}} = \sum a_n z^n,
	\]
	ens preguntem quin són els coeficients $a_n$. En altres paraules, ens preguntem pel valor de:
	\[
		[z^n] (1+z+z^2+...) \cdots (1+z+z^2+...),
	\]
	on aquest producte té $m+1$ termes idèntics. Això és equivalent a comptar el nombre de solucions
	de l'equació $\alpha_1 + \alpha_2 + ... + \alpha_{m+1} = n$, $\alpha_i \in \z_{\geq 0}$.
	Aquest problema és conegut, i dona com a resultat $\binom{n+m}{n}$.
\end{example}

\section{Classes combinatòries}

\begin{defi}[classe!combinatòria]
	Una classe combinatòria $\mathcal{A}$ és un conjunt numerable d'objectes, tals que cada
	objecte té definida una mida:
	\[
		\begin{aligned}
			|\cdot| \colon \mathcal{A} &\to \n \\
			x &\mapsto |x|,
		\end{aligned}
	\]
	de manera que el nombre $a_n$ d'objectes en $\mathcal{A}$ de mida $n$ és finit per a tot $n$. 
\end{defi}

\begin{defi}[funció!generadora]
	La funció generadora de la classe combinatòria $\mathcal{A}$ és:
	\[
		A(z) = \sum_{\alpha \in \mathcal{A}} z^{\abs{\alpha}}= \sum_{n \geq 0} a_n z^n.
	\]
\end{defi}

\begin{example}
	\begin{enumerate}[i)]
		\item[]
		\item Els naturals poden ser equipats amb estructura de classe combinatòria definint la
		mida d'un natural $n \in \n$ com a $|n| = n$. En aquest cas:
		\[
			N(z) = 1 + z + z^2 + \dots
		\]
		\item La classe $\mathcal{P}$ de permutations, on els objectes són permutacions de $[n]$
		per a cert $n$, són una classe combinatòria elegint, per exemple, $|\sigma| = n$:
		\[
			P(z) = 1 + z + 2z^2 + 3! z^3 + \dots
		\]
	\end{enumerate}
\end{example}

\begin{defi}
	Es poden definir operacions amb classes combinatòries:
	\begin{enumerate}
		\item La suma $\mathcal{A} + \mathcal{B}$ es defineix com la unió disjunta
		de $\mathcal{A}$ i $\mathcal{B}$.
		\item El producte $\mathcal{A} \times \mathcal{B}$ és el producte cartesià dels
		conjunts, amb la mida $|(\alpha,\beta)| = |\alpha| + |\beta|$.
		\item La seqüència d'una classe combinatòria $\mathcal{A}$ és:
		\[
			\seq(\mathcal{A}) = \mathcal{E} + \combA + \combA \times \combA + \combA \times \combA \times \combA + \dots
		\]
		$\mathcal{E}$ denota una classe amb un únic element de mida zero. 
		Per garantir que $\seq(\combA)$ sigui una classe combinatòria, és necessari demanar que $a_0 = 0$.
	\end{enumerate}
\end{defi}

\begin{prop}
	\begin{enumerate}
	\item[]
	\item Si $\combC_1 = \combA + \combB$, aleshores $C_1(z) = A(z) + B(z)$.
	\item Si $\combC_2 = \combA \times \combB$, aleshores $C_2(z) = A(z)B(z)$.
	\item Si $\combC_3 = \seq(\combA)$, aleshores $C_3(z) = \frac{1}{1-A(z)}$.
	\end{enumerate}
\end{prop}
\begin{proof}
	La suma és immediata a partir de la definició. Pel que fa al producte:
	\[
		C_2(z) = \sum_{(\alpha,\beta) \in \combA \times \combB} z^{|\alpha| + |\beta|}
		= \left(\sum_{\alpha \in \combA} z^{|\alpha|}\right) \left(\sum_{\beta \in \combB} z^{|\beta|}\right)
		= A(z) B(z).
	\]
	Finalment, per la seqüència:
	\[
		C_3(z) = 1 + A(z) + (A(z))^2 + \dots = \frac{1}{1-A(z)}.
	\]
\end{proof}

\section{Exemples bàsics}
\subsection{Composicions d'enters}

\begin{defi}[composició d'un enter]
	Una composició d'un enter positiu $n$ és una $k$-tupla $(x_1,\dots,x_k)$, amb $x_i$
	enters positius, tals que $x_1 + \cdots + x_k = n$.
\end{defi}

Podem construir la classe combinatòria de composicions $\combC$, on els objectes són $k$-tuples
$\alpha = (x_1,\dots,x_k)$ amb $|\alpha| = x_1 + \cdots + x_k$. Observem que
\[
	\combC = \seq(\n),
\]
considerant $\n$ sense el zero. Aleshores,
\[
	C(z) = \frac{1}{1-N(z)} = \frac{1}{1-\frac{z}{1-z}} = \frac{1-z}{1-2z}.
\]
Així doncs, comptar el nombre de composicions d'un enter $n$ es redueix a calcular $[z^n] C(z)$.
Es té:
\[
	C(z) = (1-z) \sum_{n \geq 0} 2^n z^n = \sum_{n \geq 0} 2^n z^n - \sum_{n \geq 0} 2^n z^{n+1}
	= 1 + \sum_{n\geq 1} 2^{n-1}z^n.
\]
Així, el nombre de composicions de $n$ és $2^{n-1}$, com es podria haver obtingut amb arguments directes
de caire combinatori.

Amb aquest mateix llenguatge, es poden considerar diferents variants del problema original:
\begin{itemize}
	\item \textit{De quantes maneres es pot composar $n$ en exactament $k$ parts?} En aquest
	cas, la nostra classe és:
	\[
		\combC^{(k)} = \n^k \implies C^{(k)} (z) = \left( \frac{z}{1-z}\right)^k.
	\]
	A partir d'aquí, no és difícil veure que $c_{n}^k = \binom{n-1}{k-1}$.
	\item \textit{De quantes maneres es pot composar $n$ amb parts senars?} En aquest
	cas, la classe dels enters positius senars $2 \n + 1$ té com a funció generadora:
	\[
		O(z)=  z + z^3 + z^5 + \cdots.
	\]
	Així, la classe de composicions en nombres senars satisfà:
	\[
		\combC^{(odd)} = \seq(2 \n + 1) \implies C^{(odd)} = \frac{1}{1-O(z)} = \frac{1 - z^2}{1-z-z^2}.
	\]
	Es podria obtenir una expressió per la funció generadora descomposant l'obtinguda en fraccions simples.
	\item \textit{De quantes maneres es pot composar $n$ en un nombre senar de parts?} Igual que
	abans, es té:
	\[
		\combC = \n + \n^3 + \dots \implies C(z) = N(z) + N(z)^3 + \dots
		= \frac{N(z)}{1-N^2(z)} = \frac{\frac{z}{1-z}}{1-\left(\frac{z}{1-z}\right)}.
	\]
\end{itemize}

\subsection{Particions d'enters}

\begin{defi}[partició!d'un enter]
	Una partició d'un enter positiu $n$ és una expressió del tipus $n = \alpha_1 + \dots + \alpha_k$,
	amb $1 \leq \alpha_1 \leq \dots \leq \alpha_k$, $k \geq 1$.
\end{defi}

La classe combinatòria $\mathcal{P}$ de les particions d'enters positius està formada
per $k$-tuples $(\alpha_1,\dots,\alpha_k)$, satisfent $1 \leq \alpha_1 \leq \dots \leq \alpha_k$
i definint la mida com a $|(\alpha_1,\dots,\alpha_k)| = \alpha_1 + \dots + \alpha_k$.
Observem que les particions estan formades per una seqüència de uns (potser buida),
seguida per una seqüència de dosos, de tresos... En altres paraules:
\[
	\mathcal{P} = \seq(\{ 1\}) \times \seq(\{ 2\}) \times \dots
\]
Per tant,
\[
	P(z) = \frac{1}{1-z} \cdot \frac{1}{1-z^2} \cdot \frac{1}{1-z^3} \cdots = \prod_{k=1}^{\infty} \frac{1}{1-z^k}.
\]
En general, si restringim que els elements de la partició pertanyin a un cert $A \subseteq \n$,
\[
	P_A (z) = \prod_{a \in A} \frac{1}{1-z^a}.
\]
Es té el següent resultat, obtingut per Schur, que dona una fita asimptòtica de les particions de $n$
amb parts a un cert conjunt finit:
\begin{teo*}
	Donat $A = \{a_1,\dots,a_M \}$ tal que $\gcd(a_1,\dots,a_M) = 1$, aleshores:
	\[
		[z^n] \prod_{k=1}^{M} \frac{1}{1-z^{a_k}} \sim \frac{n^{M-1}}{(M-1)! a_1 \cdots a_M}.
	\]
\end{teo*}
\begin{proof}
	No es farà la demostració en detall, sinó que es donarà la idea general de la prova. Observem que:
	\[
		(1-z^{a_k}) = C (1-z) (1-\frac{z}{w}) \cdots (1-\frac{z}{w^{a_k -1}}),
	\]
	sent $w$ una arrel $a_k$-èsima primitiva de la unitat i $C$ una constant. En el producte de tots els
	$(1-z^{a_k})$ amb la descomposició donada, 1 hi apareixerà amb multiplicitat exactament $M$, i
	la resta d'arrels hi apareixeran amb multiplicitat estrictament menor (per coprimalitat). Així,
	descomposant en fraccions simples:
	\[
		P_A (z) = \lp \frac{b_{11}}{(1-z)^M} + \dots + \frac{b_{M1}}{(1-z)} \rp + \lp
		\frac{b_{12}}{(1-\frac{z}{w})^{\alpha_w}} + \dots \frac{b_{\alpha_w 2}}{(1-\frac{z}{w})}
		\rp + \dots
	\]
	amb $\alpha_w < M$. Per concloure la demostració, és convenient fer la consideració:
	\[
		\frac{1}{(1-\frac{z}{w})^{M}} = \sum_{n \geq 0} \binom{n+M-1}{M-1} z^n
	\] 
	i tenir en compte que, per $n >> M$, $\binom{n+M-1}{M-1} \sim \frac{n^{M-1}}{(M-1)!}$.
\end{proof}

\subsection{Particions de conjunts}
\begin{defi}[partició!d'un conjunt]
	Una partició d'un conjunt $A$ és una descomposició en subconjunts tal que
	$A = A_1 \cup \dots \cup A_k$ i $A_i \cap A_j = \emptyset$ si $i \neq j$.
\end{defi}

Així, si $(b_1, \dots, b_k)$ són els blocs d'una partició de $[n]$, sempre els podem 
ordenar de manera que $\min b_1 < \min b_2 < \dots < \min b_k$. De la mateix manera, podem
expressar una partició a través d'una paraula $x_1 \dots x_n$, tal que $x_i = b_j \iff i \in b_j$. 
Per exemple, si $b_1 = \{1,4,6\}$, $b_2 = \{2,3\}$, $b_3 = \{5\}$, la paraula corresponent seria
$b_1 b_2 b_2 b_1 b_3 b_1$. 

Observem que no totes les paraules són admissibles: $b_1 b_3 b_2 \dots$ no és possible
ja que qualsevol aparició de $b_{i+1}$ ha d'estar precedida per una aparició de $b_i$.
Per tant, si $\Pi_k$ és la classe combinatòria de les particions en $k$ parts, on els
elements són paraules formades per $b_1,\dots,b_k$ (i la mida és la longitud de la paraula),
es té:
\[
	\Pi_k  = \{b_1 \} \times \seq(\{b_1 \}) \times \{b_2\} \seq(\{b_1,b_2 \}) \times \dots \times \{b_k\} \seq(\{b_1 ,\dots ,b_k\}).
\]
La funció generadora corresponent és:
\[
	\Pi_k (z) = z \cdot \frac{1}{1-z} \cdot z \cdot \frac{1}{1-2z} \cdots z \cdot \frac{1}{1-kz} 
	= \frac{z^k}{(1-z) \cdots (1-kz)}.
\]

\begin{defi}
	Els nombres de Stirling de segona classe, $\stirling{n}{k}$, són els coeficients de la funció
	generadora de $\Pi_k$:
	\[
		\sum_{n \geq 0} \stirling{n}{k} z^n.
	\]
	En altres paraules, són el nombre particions de $\{1,\dots,n\}$ en $k$ parts.
\end{defi}

Hom pot comprovar que
\[
	\stirling{n}{k} = \frac{1}{k!} \sum_{j = 1}^{k} (-1)^{k-j} \binom{k}{j} j^n.
\]

\subsection{Camins de Dyck}

\begin{defi}[camí de Dyck]
	Un camí de Dyck és una seqüència de punts al pla començant a $(0,0)$ i acabant
	$(2n,0)$, amb passes ascendents $\nearrow = (1,1)$ o descendents $\searrow = (1,-1)$,
	de manera que tots els punts tenen component $y$ no negativa.
\end{defi}

Observem que tots els camins han de començar necessàriament amb un pas $\nearrow$.
Anàlogament, si en algun moment s'arriba a l'origen, s'hi ha d'arribar amb un pas $\searrow$.
Si ens fixem en el camí entre l'origen i la primera vegada que es toca l'eix $\{y=0\}$,
el camí ha de ser una passa $\nearrow$, seguida per un camí de Dyck, seguida
per una passa $\searrow$. La resta del camí serà un altre camí de Dyck. Així, es pot escriure:
\[
	\mathcal{D} = \mathcal{E} + \{ \nearrow \} \times \mathcal{D} \times \{ \searrow\} \times \mathcal{D}.
\]
En altres paraules, un camí de Dyck es pot dividir en dos camins més petits. La funció generadora satisfà:
\[
	D(z) = 1 + z^2 D^2 (z).
\]
Una manera d'aïllar $D(z)$ seria a través de resoldre l'equació de segon grau corresponent:
\[
	D(z) = \frac{1 \pm \sqrt{1 - 4z^2}}{2z^2}.
\]
L'arrel quadrada es pot desenvolupar en sèrie de potències, i raonant que $\lim_{z \to 0^+} D(z) = 1$,
podríem deduir que hauríem d'escollir la solució amb signe negatiu. Una manera més eficient d'obtenir
resultats és la fórmula d'inversió de Lagrange:

\begin{teo}[Fórmula d'inversió de Lagrange]
    \label{teo:FIL}
	Sigui $\phi$ una sèrie formal de potències amb $\phi_0 \neq 0$. Si $Y(z)$ és una sèrie
	formal de potències satisfent:
	\[
		Y(z) = z \phi(Y(z)),
	\]
	aleshores $[z^n] Y(z) = \frac{1}{n} [t^{n-1}] (\phi(t))^n$.
\end{teo}

La demostració la farem més endavant. Observem que si $V(z) = z D(z)$, aleshores:
\[
	V(Z) = z (1 + V^2(z)).
\]
Aplicant la fórmula d'inversió de Lagrange amb $\phi(t) = 1+t^2$:
\[
	[z^n] V(z) = \frac{1}{n} [t^{n-1}] (1+t^2)^n = \begin{cases}
		\frac{1}{n} \binom{n}{\frac{n-1}{2}} & \text{ si } n \text{ és imparell,} \\
		0 & \text{ si } n \text{ és parell.} \\
	\end{cases}
\]
En conseqüència:
\[
	 [z^{2m}] D(z) = \frac{1}{2m} \binom{2m-1}{m} = \frac{1}{m+1} \binom{2m}{m} = C_m.
\]
\begin{defi}[nombres!de Catalan]
	El $m$-èsim nombre de Catalan es defineix com $C_m = \binom{2m}{m}$.
\end{defi}

En resum,
\[
	D(z) = \sum_{m \geq 0} C_m z^{2m}.
\]

\section{Arbres plans}

\begin{defi}[arbre!pla]
    Un arbre pla és un arbre (graf connex acíclic) dibuixat al pla, arrelat, i tal que
    l'ordre dels subarbres que pengen de cada node és rellevant.
\end{defi}
%TODO: un dibuixet hehe
Sigui $\mathcal{T}$ la classe dels arbres plans, on la mida és el nombre de nodes. 
Aleshores, se satisfà:
\[
    \mathcal{T} = \mathcal{N} \times \seq(\mathcal{T}),
\]
sent $\mathcal{N}$ la classe que conté un únic objecte de mida 1 (un únic node). Observem que
\[
    T(z) = z \frac{1}{1-T(z)}.
\]
Si apliquem la fórmula d'inversió de Lagrange amb $\phi(t) = \frac{1}{1-t}$, obtenim:
\[
    [z^n] T(z) = \frac{1}{n} [t^{n-1}] \frac{1}{(1-t)^n} = \frac{1}{n} \binom{2n-2}{n-1} = C_{n-1}.
\]
Per tant, obtenim el $n-1$-èsim nombre de Catalan.

\begin{defi}[arbre!binari]
    Un arbre binari és un arbre pla tal que cada node té zero o dos fills.
\end{defi}

Observem que un arbre binari és o bé un node sol o bé una arrel de la qual pengen dos arbres binaris.
Si $\combB$ és la classe dels arbres binaris, sent la mida el nombre de vèrtexs interiors
(és a dir, els vèrtexs que tenen fills), obtenim:
\[
    \combB = \mathcal{E} + \mathcal{N} \times \combB \times \combB.
\]
Per tant,
\[
    B(z) = 1 + z B^2 (z) \implies (B(z) -1) = z(1 + (B(z)-1))^2.
\]
Si apliquem la fórmula d'inversió de Lagrange amb $\phi(t) = (1+t)^2$ i amb $B(z)-1$, obtenim:
\[
    [z^n] B(z) = \frac{1}{n} [t^{n-1}] (1+t)^{2n} = \frac{1}{n} \binom{2n}{n-1} = C_n.
\]
Obtenim altra vegada un nombre de Catalan.
\begin{ej}
    Penseu en com podríem construir un isomorfisme entre arbres plans i arbres binaris.
\end{ej}

\section{Fórmula d'inversió de Lagrange}
En aquesta secció, demostrarem la fórmula d'inversió de Lagrange. Abans, però, necessitem
definir uns conceptes previs:

\begin{defi}
    Siguin $A(z)$ i $B(z)$ dues sèries formals de potències, amb $b_0 = 0$. 
    La seva composició es defineix com a:
    \[
        A(B(z)) = \sum_{n \geq 0} a_n (B(z))^n.
    \]
\end{defi}

Observem que:
\[
    [z^m] A(B(z)) = \sum_{n \geq 0} a_n [z^m] (B(z))^n = \sum_{n = 0}^m a_n [z^m] (B(z))^n.
\]
Aquesta suma sempre és finita, ja que hem assumit que $b_0 = 0$. Sinó, el càlcul
de $[z^0] A(B(z))$ podria donar lloc a una sèrie de nombres complexos, que podria ser divergent.

\begin{defi}
    Una sèrie (formal) de Laurent és una expressió de la forma:
    \[
        A(z) = \sum_{n \geq -k} a_n z^n = \frac{a_{-k}}{z^k} + \frac{a_{-(k-1)}}{z^{k-1}}  + \dots
        + \frac{a_{-1}}{z} + a_0 + a_1 z + \dots
    \]
    per a un cert $k$.
\end{defi}

\begin{prop}
    Amb la suma component a component i el producte de convolució, el conjunt de sèries
    de Laurent (denotat per $\cx((z))$) és un cos. De fet, és el cos de fraccions de $\cx[[z]]$.
\end{prop}
\begin{proof}
    Només cal comprovar que tot element diferent del zero té invers pel producte a $\cx((z))$.
    Si $k > 0$, i tenim una sèrie del següent estil:
    \[
        A(z) = \sum_{n \geq k} a_n z^n = z^k (a_k + a_{k+1} z + \dots) = z^k A_1(z),
    \]
    sent $A_1(z)$ invertible en virtut de la proposició \ref{prop:InvSeries}. Per tant,
    si $B_1(z) = \frac{1}{A_1(z)}$ és la inversa, podem escriure:
    \[
        \frac{1}{A(z)} = \frac{1}{z^k A_1 (z)} = \frac{1}{z^k} B_1(z). 
    \]
    Per tant, hem trobat una inversa a $A(z)$ en $\cx((z))$. Si $A(z)$ no pertany a
    $\cx [[z]]$, el raonament és anàleg.
\end{proof}

\begin{defi}
    Donada una sèrie de Laurent $A(z)$, el seu residu és $\Res(A(z)) = [z^{-1}] A(z)$.
\end{defi}

\begin{defi}
    La derivada d'una sèrie formal de potències $A(z) = \sum_{n \geq -k} a_n z^n$ és:
    \[ 
        A'(z) = \sum_{n \geq -k} n a_n z^{n-1}.
    \]
\end{defi}

Observem que $[z^{-1}] A'(z) = 0 a_0 = 0$. És a dir, si una sèrie de Laurent és derivada
d'una altra sèrie, aleshores el seu residu és 0.

\begin{lema}
    Si $A(z) = \sum_{n \geq -k} a_n z^n$, aleshores:
    \[
        \Res \lp \frac{A'(z)}{A(z)}\rp = -k.
    \]
\end{lema}
\begin{proof}
    Si $A(z) = \frac{a_{-k}}{z^k} + \dots$, aleshores podem escriure:
    \[
        \begin{aligned}
            A'(z) = \frac{-ka_{k}}{z^{k+1}} + \dots \\
            \frac{1}{A(z)} = \frac{1}{a_k z^k} + \dots
        \end{aligned}
    \]
    Per tant, el coeficient de $[z^{-1}]$ del producte de les sèries serà $-k$, com
    volíem veure.
\end{proof}

\begin{teo*}
    Siguin $A(z),B(z)$ sèries formals de potències amb $a_0 = b_0 = 0$, $b_1 \neq 0$, tals que
    \[
        A(B(z)) = z.
    \]
    Aleshores:
    \[
        [z^n] A(z) = \frac{1}{n} \Res \lp \frac{1}{(B(z))^n}\rp
    \]
\end{teo*}
\begin{proof}
    La condició de l'enunciat és:
    \[
        A(B(z)) = \sum_{n \geq 1} a_n (B(z))^n = z.
    \]
    Diferenciant, s'obté:
    \[
        \sum_{n \geq 1} n a_n (B(z))^{n-1} B'(z) = 1.
    \]
    Per a un cert $m$ fixat, dividim els dos costats per $(B(z))^m$:
    \[
        \sum_{n \geq 1} n a_n (B(z))^{n-m-1} B'(z) = \frac{1}{B(z)^m}.
    \]
    Tot seguit, prenem residu als dos membres. Al terme de l'esquerra, observem
    que si $n \neq m$, llavors $(B(z))^{n-m-1} B'(z) = (B(z)^{n-m})'$ és una derivada,
    i per tant té residu zero. Si $n = m$, pel lema anterior, $\frac{B'(z)}{B(z)}$ té residu
    $-1$. Per tant,
    \[
        n a_m = \Res \lp \frac{1}{(B(z))^m}\rp,
    \]
    com volíem veure.
\end{proof}

Ara ja estem en condicions de demostrar la fórmula d'inversió de Lagrange, enunciada a \ref{teo:FIL}:
\begin{proof}
    Suposem que $Y(z) = z \phi(Y(z))$. Aleshores, si $B(t) = \frac{t}{\phi(t)}$, se satisfà $B(Y(z)) = z$.
    Pel teorema anterior, es té:
    \[
        [z^n] Y(z) = \frac{1}{n} [t^{-1}] \lp \frac{t}{\phi(t)} \rp^n = \frac{1}{n} [t^{n-1}] \frac{1}{\phi(t)^n}.
    \]
\end{proof}

Una extensió de la fórmula d'inversió de Lagrange, que no demostrarem aquí, és la fórmula
de Bürmann-Lagrange:
\begin{teo*}
    Sota les condicions de la fórmula d'inversió de Lagrange, si $g$ és una funció analítica, es té:
    \[
        [z^n] g(Y(z)) = \frac{1}{n} [t^{n-1}] g'(t) \phi(t)^n.
    \]
\end{teo*}
