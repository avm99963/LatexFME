# Descripció
 
Repositori d'apunts a Latex. Cada assignatura té una carpeta pròpia.

#### Obligatori llegir [normas_latex.md](https://github.com/ApuntsFME/Normes/blob/master/normas_latex.md), [commits.md](https://github.com/ApuntsFME/LatexFME/blob/master/commits.md) y [tutorial_git.md](https://github.com/ApuntsFME/Normes/blob/master/tutorial_git.md) abans d'editar el repositori.

## Treballar amb git amb submodules

Git permet treballar amb un repositori dins d'un altre repositori. Git es guardarà l'URL del submòdul (el repositori que està a dins d'un altre) i el commit que estem rastrejant (és a dir, el commit on estem "checked out" al submòdul). Si volem canviar el commit rastrejat, caldrà especificar-ho al repositori que el conté. A continuació s'exposen les comandes més útils.
Per clonar el repositori amb els submòduls:
```bash
git clone --recursive
```
Equivalentment, podem usar l'opció `--recurse-submodules` enlloc de `--recursive`.

Si ja està clonat el repositori però ens hem oblidat d'afegir el `--recursive`, no es descarregaran els arxius del submòdul, per tal de descarregar-los, correu la següent comanda:
```bash
git submodule update --init --recursive
```

Per fer pull del repositori incloent els submòduls (a l'últim commit rastrejat):
```bash
git pull --recurse-submodules
```

Per actualitzar el commit que estem rastrejant (cal executar-ho des de fora de la carpeta de submòdul):
```bash
git submodule update --remote --rebase
```

## Títols massa llargs

Si el títol d'un capítol és massa llarg i no cap als headers de les pàgines, podem declarar-lo de forma curta de la següent forma:
```
\chapter[Títol curt]{Títol llarg}
```

Si ens passa a una secció d'un capítol, es pot fer així:
```
\section[Títol llarg]
    {Títol llarg
    \sectionmark{Títol curt}}
    \sectionmark{Títol curt}
```

## Atenció
Qualsevol canvi al procediment del repositori ha de ser sotmès a votació als editors d'aquest. 
Els suggeriments i possibles ampliacions es poden afegir al fitxer anomenat suggeriments 
que trobareu a aquesta carpeta.

#### SOCIALISME O BARBÀRIE (ups, se m'ha colat).
#### UNIVERSO SIMPSON O BARBARIE (ups, ya van dos).
Altres links d'interès: 
 * [Loose Canon (first edition)](http://www.loose-canon.info/Loose-Canon-1st-Ed.pdf)
 * [EMOJI Cheatsheet](https://www.webpagefx.com/tools/emoji-cheat-sheet/) 
