\section{Successions i sèries funcionals}
\begin{defi}
    Sigui $\lp f_n\rp$ una successió de funcions, $f_i\colon E\subseteq \real \to \real$, i sigui $f\colon A\subseteq E\to \real$. Diem que $\lp f_n\rp$ convergeix puntulament cap a $f$ en $A$ si
    \[
        \lim_{n\to \infty} f_n\lp x\rp = f\lp x\rp,\, \forall x\in A.
    \]
    Diem que $\lp f_n\rp$ convergeix uniformement cap a $f$ en $A$ si
    \[
        \forall \varepsilon > 0, \, \exists N\in\n \TQ \abs{f_n\lp x\rp - f\lp x\rp} <\varepsilon, \, \forall n\geq N, \, \forall x\in A.
    \]
\end{defi}
\begin{obs}
    $\lp f_n\rp$ convergeix uniformement cap a $f$ en $A$ si, i només si, 
    \[
        \lim_{n\to \infty} \sup_{x\in A} \abs{f_n\lp x\rp - f\lp x\rp} = 0.
    \]
    
\end{obs}
\begin{obs}
    La convergència uniforme implica la convergència puntual.
\end{obs}
\begin{prop}[Criteri de Cauchy]
    Sigui $\lp f_n\rp$ una successió de funcions, $f_i\colon E\subseteq \real \to \real$, i sigui $f\colon A\subseteq E\to \real$.
    $\lp f_n\rp$ convergeix uniformement cap a $f$ en $A$ si, i només si, 
    \[
        \forall \varepsilon > 0, \, \exists N\in\n \TQ \abs{f_n\lp x\rp - f_m\lp x\rp} <\varepsilon, \, \forall n, m\geq N, \, \forall x\in A.
    \]
\end{prop}
\begin{teo*}
    Sigui $\lp f_n\rp$ una successió de funcions contínues que convergeix uniformement cap a $f$ en $A$. Aleshores, $f$ és contínua en $A$.
\end{teo*}
\begin{lema}[Lema de Dini]
    Sigui $\lp f_n\rp$ una successió de funcions puntualment monòtona (no estrictament) que convergeix puntualment cap a $f$ en un compacte $K$. Aleshores, $\lp f_n\rp$ convergeix uniformement cap a $f$.
\end{lema}
\begin{teo*}
    Sigui $\lp f_n\rp$ una successió de funcions integrables Riemann a $\lb a, b\rb$ que convergeix uniformement cap a $f$ en $\lb a, b\rb$. Aleshores, $f$ és integrable Riemann a $\lb a, b\rb$ i
    \[
        \int_a^b f = \int_a^b \lim_{n\to \infty} f_n = \lim_{n\to \infty} \int_a^b f_n.
    \]
\end{teo*}
\begin{col}
    Sota les mateixes condicions, si $x \in \lb a, b\rb$,
    \[
        \lp \int_a^x f_n \rp \to \int_a^x f
    \]
    uniformement.
\end{col}
\begin{teo*}
    Sigui $\lp f_n\rp$ una successió de funcions derivables en $\lb a, b\rb$ tal que $\lp f_n^{\prime}\rp$ convergeix uniformement cap a $g$ en $\lb a, b\rb$ i tal que $\lp f_n\lp x_0\rp \rp$ convergeix per un cert $x_0\in \lb a, b\rb$. Aleshores, $\lp f_n\rp$ convergeix uniformement cap a una funció $f$ derivable a $[a, b]$ que satisfà que $f^{\prime}=g$. En particular,
    \[
        \lim_{n\to \infty} f_n^{\prime} = \lp \lim_{n\to \infty} f_n\rp ^{\prime}.
    \]
\end{teo*}
\emph{Comentari}: quan no s'especifiqui el domini de les funcions ni en quin subconjunt del domini tenen certes propietats, s'entendrà que tot passa en un mateix subconjunt qualsevol dels reals.
\begin{prop}[Criteri de Weierstrass]
    Sigui $\lp f_n\rp$ una successió de funcions tal que, per tot $n$, $\abs{f_n}\leq M_n$, per a cert $M_n\in \real$ i tal que $\sum_{n\geq 1} M_n$ és convergent. Aleshores, $\sum f_n$ convergeix uniformement.
\end{prop}
\begin{lema}[Fórmula de sumació d'Abel]
    Siguin $\lp a_n\rp, \lp b_n\rp$ successions de nombres reals i sigui $s_n=\sum_{i=1}^n a_i$. Aleshores,
    \[
        \sum_{i=1}^n a_ib_i = s_nb_{n+1} - \sum_{i=1}^n s_i\lp b_{i+1}-b_i\rp = s_nb_1 + \sum_{i=1}^n\lp s_n - s_i\rp \lp b_{i+1}-b_i\rp.
    \]
    Aquest és un lema auxiliar que s'utilitza per demostrar els següents dos criteris.
\end{lema}
\begin{prop}[Test d'Abel]
    Siguin $\lp f_n\rp$ i $\lp g_n\rp$ successions de funcions tals que
    \begin{enumerate}[(i)]
        \item $\sum f_n$ convergeix uniformement,
        \item $\forall n, \, \abs{g_n} < M$,
        \item $\lp g_n\rp$ és puntualment monòtona (no estrictament).
    \end{enumerate}
    Aleshores, $\sum f_ng_n$ convergeix uniformement.
\end{prop}
\begin{prop}[Test de Dirichlet]
    Siguin $\lp f_n\rp$ i $\lp g_n\rp$ successions de funcions tals que
    \begin{enumerate}[(i)]
        \item $\forall n, \, \abs{\sum_{i=1}^n f_i}<M$, és a dir, que $\sum f_n$ és uniformement fitada.
        \item $\lp g_n\rp$ és puntualment monòtona (no estrictament),
        \item $\lp g_n\rp$ convergeix uniformement a $0$.
    \end{enumerate}
    Aleshores, $\sum f_ng_n$ convergeix uniformement.
\end{prop}
\begin{defi}
    Anomenem sèrie de potències a una expressió de la forma
    \[
        \sum_{n\in \n} a_n\lp x-a\rp ^n.
    \]
    El canvi de variable $y=x-a$ és un desplaçament que transforma l'expressió en
    \[
        \sum_{n\in \n} a_n y ^n,
    \]
    de manera que només considerarem sèries de potències d'aquesta forma.
\end{defi}
\begin{obs}
    Observem que una sèrie de potències $\sum_{n\in\n} a_nx^n$ defineix la successió de funcions $\lp \sum_{i=1}^n a_nx^n\rp$. Quan parlem de la convergència de la sèrie ens referirem a la convergència d'aquesta successió de funcions que defineix.
\end{obs}
\begin{prop}
    Sigui $\sum_{n\in\n}a_nx^n$ una sèrie de potències. Aleshores,
    \begin{enumerate}[(i)]
        \item Si $\sum_{n\in\n} a_nx^n$ convergeix en $x_0\neq 0$, aleshores convergeix absolutament en tot $x\in \lp -\abs{x_0}, \abs{x_0}\rp$. A més, convergeix uniformement en tot $\lb a, b\rb \subset \lp -\abs{x_0}, \abs{x_0}\rp$.
        \item Si $\sum_{n\in\n} a_nx^n$ divergeix en $x_0$, aleshores divergeix en tot $x\in \real$ tal que $\abs{x}>\abs{x_0}$.
    \end{enumerate}
\end{prop}
\begin{defi}
    Anomenem radi de convergència de la sèrie de potències $\sum_{n\in\n} a_nx^n$ a
    \[
        R=\frac{1}{\limsup\limits_{n\to\infty} \sqrt[n]{\abs{a_n}}}\in \lb 0, +\infty\rb.
    \]
\end{defi}
\begin{prop}
    Sigui $\sum_{n\in\n} a_nx^n$ una sèrie de potències i sigui $R\in \lb 0, +\infty\rb$ el seu radi de convergència. Aleshores,
    \begin{enumerate}[(i)]
        \item $\sum_{n\in\n} a_nx^n$ convergeix a $\lp -R, R\rp$,
        \item $\sum_{n\in\n} a_nx^n$ divergeix a $\lp -\infty, R\rp \cup \lp R, +\infty\rp$.
    \end{enumerate}
    El resultat també val canviant $R$ per $-R$ en les sèries i considerant $\lb -R, 0 \rb$ i $\lim\limits_{x\to -R^+}$.
\end{prop}
\begin{teo}[Teorema d'Abel]
     Sigui $\sum_{n\in\n} a_nx^n$ una sèrie de potències amb radi de convergència $R\in \lb 0, +\infty\rp$ tal que $\sum_{n\in\n} a_nx^n$ convergeix a $R$, és a dir, si $\sum_{n\in\n} a_nR^n=A$. Aleshores,
    \begin{enumerate}[(i)]
        \item $\sum\limits_{n\in\n} a_nx^n$ convergeix uniformement a $\lb 0, R\rb$,
        \item $\lim\limits_{x\to R^{-}} \sum\limits_{n\in\n} a_nx^n = A$.
    \end{enumerate}
\end{teo}
\begin{obs}
    Sigui $\sum_{n\in\n} a_nx^n$ una sèrie de potències. Observem que $\sum_{n\in\n} a_nx^n$ defineix una funció
    \begin{align*}
        f\colon \lp -R, R\rp &\to \real\\
        x &\mapsto \sum_{n\in\n} a_nx^n.
    \end{align*}
\end{obs}
\begin{lema}
    Les sèries de potències $\sum_{n\in\n} a_nx^n$ i $\sum_{n\in\n} na_nx^{n-1}$ tenen el mateix radi de convergència.
\end{lema}
\begin{prop}
    Sigui $\sum_{n\in\n} a_nx^n$ una sèrie de potències i sigui $R$ el seu radi de convergència. Aleshores, $f\lp x\rp=\sum_{n\in\n} a_nx^n$ és derivable a $\lp -R, R\rp$ i
    \[
        f^{\prime}\lp x\rp = \sum_{n\in \n} na_nx^{n-1}.
    \]
\end{prop}
\begin{prop}
    Sigui $\sum_{n\in\n} a_nx^n$ una sèrie de potències. Aleshores, 
    \begin{enumerate}[(i)]
        \item $\displaystyle f\lp x\rp=\sum_{n\in\n} a_nx^n$ és integrable Riemann a $\lp -R, R\rp$,
        \item $\displaystyle\sum_{n\in \n}\int_{x_0}^x a_n t^n \dif t$ és una sèrie de potències en la variable $x$ amb radi de convergència $R$, per tot $x_0 \in \lp -R, R\rp$,
        \item $\displaystyle \int_{x_0}^x \sum_{n\in\n} a_nt^n \dif t = \sum_{n\in \n}\int_{x_0}^x a_n t^n \dif t$, per tot $x_0, x \in \lp -R, R\rp$.
    \end{enumerate}
\end{prop}
\begin{teo}[Teorema de Taylor]
    Sigui $\sum_{n\in\n} a_nx^n$ una sèrie de potències que convergeix a $f\lp x\rp$ a la regió $\lp -R, R\rp$ i sigui $c\in\lp -R, R\rp$. Aleshores,
    \[
        f\lp x\rp = \sum_{n\in\n} \frac{f^{\lp n\rp}\lp c\rp}{n!}\lp x-c\rp^n
    \]
    per a tot $x$ tal que $\abs{x-c}<R-\abs{c}$.
\end{teo}
\begin{teo*}
    Sigui $f$ una funció suau a $\lp a, b\rp$ tal que
    \[
        \abs{f^{\lp n\rp}\lp x\rp} \leq \gamma, \, \forall x\in\lp a, b\rp, \, \forall n
    \]
    per a cert $\gamma>0$. Aleshores, $f$ té una sèrie de Taylor de radi no nul al voltant de cada punt $c\in \lp a, b\rp$.
\end{teo*}
