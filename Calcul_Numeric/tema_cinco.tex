\chapter{Métodos numéricos para EDOs}

\section{Introducción}
Veremos métodos para aproximar la solución de Problemas de Valor Inicial (PVI)
de forma
\[
    \dv{\bar{y}}{x} = \bar{f}\left( x, \bar{y} \right) \quad \bar{y}(a) =
    \bar{\alpha}
\]
Siendo la EDO cierta para $x > a$.

\begin{example}
    \begin{enumerate}[1)]
        \item []
        \item
            \[
                \begin{cases}
                    y^\prime = 2xy \\
                    y(0) = 1
                \end{cases}
            \]
        \item
            \[
                \begin{cases}
                    \dv{y}{t} = 2xy \\
                    \dv{x}{t} = x + y
                \end{cases}
                \quad
                \begin{cases}
                    x(0) = 0 \\
                    y(0) = 3
                \end{cases}
            \]
            Que es equivalente a
            \[
                \begin{cases}
                    \dv{\bar{y}}{x} = \bar{f}\left( x,
                    \bar{y}    \right) \\
                    \bar{y}(a) = \bar{\alpha}
                \end{cases}
                \text{ con }
                \bar{y} =
                \begin{pmatrix}
                    x \\ y
                \end{pmatrix}
                =
                \begin{pmatrix}
                    y_1 \\ y_2
                \end{pmatrix}
                \text{ y } \bar{f}\left( t, \bar{y} \right) =
                \begin{pmatrix}
                    y_1 + y_2 \\ 2y_1y_2
                \end{pmatrix}
            \]
            Y condición inicial $\bar{\alpha} = \bar{y}(0) =
            \begin{pmatrix} 0 \\ 3 \end{pmatrix}$.
        \item
            \[
                \begin{cases}
                    y^{\prime\prime} = -y  \quad x > 0 \\
                    y(0) = 0; \, y^\prime(0) = 1
                \end{cases}
            \]
            Consideramos
            \[
                \bar{z} = \begin{pmatrix} y \\ y^\prime
                \end{pmatrix} \text{, } \bar{z}(0) = \bar{\alpha} = \begin{pmatrix} 0 \\ 1
                \end{pmatrix} \text{ y }
                \bar{f}\left(t, \bar{z}\right) =
                \begin{pmatrix}
                    z_2 \\ -z_1
                \end{pmatrix}
            \]
    \end{enumerate}
\end{example}

\begin{obs}[Notación]
    Notaremos
    \[
        \begin{cases}
            \bar{y}^i \colon= \bar{y}\left( x_i \right) \\
            f_i \colon= f\left( x_i, \bar{y}^i \right)
        \end{cases}
    \]
\end{obs}

\begin{defi}[método!numérico]
    Un método es una fórmula para calcular $\bar{Y}^{i+1} \approx
    \bar{y}\left( x_{i+1} \right)$ a partir de $\bar{Y}^i \approx
    \bar{y}\left( x_i \right)$ y si necesita, anteriores.
\end{defi}

\section{Método de Euler}

Tenemos
\[
    \dv{y}{x} (x_i) = f\left( x_i, y^i \right)
\]
Ahora, podemos aproximar $\dv{y}{x}\left( x_i \right) \approx
\frac{y^{i+1}-y^i}{h} + \O(h)$ para alguna $h$ constante. De donde obtenemos
\[
    y^{i+1} = y^i + hf\left( x_i, y^i \right) + \underbrace{\O\left( h^2
    \right)}_{\mathclap{\text{error de truncado}}}
\]
Ahora, suprimiendo los errores de truncamiento, obtenemos el método de Euler:
\[
    Y^{i+1} \approx Y^i + hf\left( x_i, Y^i \right)
\]

\begin{obs}
    \[
        y^{i+1} = y^i + \dv{y}{x}\left( x_i \right) \left( x_{i+1} - x_i
        \right) + \O\left( \abs{x_{i+1} - x_i}^2 \right)
    \]
    Por taylor centrado en $x_i$ y evaluado en $x_{i+1}$. Más en general
    \[
        y(x) = y\left( x_i \right) + \dv{y}{x}(x)\left( x - x_i \right)
        + \O\left( \abs{x - x_i}^2 \right)
    \]
    De donde sigue que
    \[
        \dv{y}{x}\left( x_i \right) = \frac{y\left( x_{i+1} \right) -
        y\left( x_i \right)}{x_{i+1} - x_i} + \O\left( \abs{x_{i+1} -
        x_i} \right)
    \]
\end{obs}

Consultar transparencias de \href{https://atenea.upc.edu/}{Atenea} para más información.

\section{Método de Euler hacia atrás}

Consideramos Taylor centrado en $x_{i+1}$
\[
    y(x) = y\left( x_{i+1} \right) + y^\prime\left( x_{i+1} \right)\left(
    x - x_{i+1} \right) + \O\left( \abs{x - x_{i+1}}^2 \right)
\]
evaluando en $x = x_i$, obtenemos
\begin{equation}\label{eq:taylorxi}
    y^i = y^{i+1} - hy^\prime\left( x_{i+1} \right) + \O\left( h^2
    \right)
\end{equation}
De donde se deduce:
\begin{itemize}
	\item Una aproximación de la derivada
		\[
			y^\prime \left( x_{i+1} \right) = \frac{y^{i+1}- y^i }{h} +
			\O(h)
		\]
	\item substituyendo la derivada en \ref{eq:taylorxi}, obtenemos
		\[
			y^{i+1} = y^i + hf\left( x_{i+1}, y^{i+1} \right) +
			\O\left( h^2 \right)
		\]
\end{itemize}

Ahora, si negligimos los errores de truncamiento, obtenemos el método de Euler
hacia atrás:
\[
	\begin{cases}
		Y^0 = \alpha \\
		Y^{i+1} - h f\left( x_{i+1}, Y^{i+1} \right) = Y^i
	\end{cases}
\]

\begin{obs}
	Hace falta resolver una ecuación (que en general no es lineal) para
	calcular $Y^{i+1}$ en cada paso. Podemos tomar $^0Y^{i+1} =
	Y^i$ como aproximación inicial.

	Si a la hora de resolver esta ecuación empleamos un método explícito, el
	método será estable. En cambio, si utilizamos un método no explícito, el
	método será condicionalmente estable, es decir, que para funcionar
	necesitará una $h$ suficientemente pequeña.
\end{obs}

\section{Método de diferencias centradas}

Hacemos Taylor centrado en $x_i$ y evaluado en:
\[
	\begin{cases}
		x = x_{i+1}: \quad y^{i+1} = y^i + hy^\prime \left( x_i
		\right) + \frac{h^2}{2} y^{\prime\prime}\left( x_i \right) +
		\O\left( h^3 \right) \\
		x = x_{i-1}: \quad y^{i-1} = y^i - h y^\prime\left( x_i
		\right) + \frac{h^2}{2}y^{\prime\prime}\left( x_i \right) +
		\O\left( h^3 \right)
	\end{cases}
\]
Restando, obtenemos
\begin{equation}
	y^{i+1} - y^{i-1} = 2hy^\prime\left( x_i \right) + \O\left( h^3
	\right)
	\label{eq:diferencesc}
\end{equation}
De donde se obtiene la aproximación centrada de la derivada:
\[
	y^\prime\left( x_i \right) = \frac{y^{i+1} - y^{i-1}}{2h} + \O\left(
	h^2 \right)
\]
O bien, si substituimos la EDO en \ref{eq:diferencesc}:
\[
	y^{i+1} = y^{i-1} + 2hf\left( x_i, y^i \right) + \O\left(h^3\right)
\]
Y menospreciando $\O\left( h^3 \right)$, obtenemos el método de las diferencias
centradas:
\[
	\begin{cases}
		Y^0 = \alpha \\
		Y^1 = Y^0 + hf\left( x_0, Y^0 \right) \\
		Y^{i+1} = Y^{i-1} + 2hf\left( x_i, Y^i \right)
	\end{cases}
\]

\section{Error local, global y orden de convergencia}

\begin{defi}[error!local]
	El error local es el error en $Y^{i+1}$, es decir,  $Y^{i+1} - y^{i+1}$
	si suponemos $Y^i$ exacto.
\end{defi}

\begin{obs}
	El error local en Euler se puede calcular suponiendo $Y^i = y^i$:
	\[
		\begin{cases}
			Y^{i+1} = y^i + hf\left( x_i, y^i \right) \\
			y^{i+1} = y^i + hf\left( x_i, y^i \right) +
			\underbrace{hT_i(h)}_{\O(h)}
		\end{cases}
	\]
	donde $T_i(h)$ es el error de truncamiento. Restando, obtenemos el error
	local:
	\[
		Y^{i+1} - y^{i+1} = hT_i(h)
	\]
\end{obs}

\begin{defi}[error!global]
	El error global es el error acumulado después de $m$ pasos con $h =
	\frac{b -a}{m}$.
\end{defi}

\begin{obs}
	Si el error local está acotado por $hT(h)$, después de $m$ pasos el
	error acumulado es
	\[
		mhT(h) = \frac{b-a}{h}hT(h)
	\]
	Por lo tanto, el error global es
	\[
		\O\left( T(h) \right)
		\begin{cases}
			\O(h) \text{ para Euler} \\
			\O \left( h^2 \right) \text{ para diferencias centradas}
		\end{cases}
	\]
\end{obs}

\begin{defi}[orden]
	Diremos que un método es de orden $q$ si el error global es $\O\left(
	h^q \right)$
\end{defi}


\begin{obs}
	Si el error local es $\O\left( h^{q+1} \right)$, entonces el error
	global es $\O\left( h^q \right)$
\end{obs}

\section[Análisis de la estabilidad de los métodos numéricos para EDOs]
    {Análisis de la estabilidad de los métodos numéricos para EDOs
    \sectionmark{Análisis de la estabilidad}}
    \sectionmark{Análisis de la estabilidad}


Consideramos $y(x) = ce^{\lambda x}$, $\lambda \in \cx$ y entonces $y^\prime =
\lambda y$. Si $\text{Re}(\lambda) < 0$, la solución tiende a 0 cuando $x \to
+\infty$.

\begin{defi}[método!estable]
	Un método es (absolutamente) estable para $\lambda$ y $h$ (con la
	consideración anterior) si
	\[
		\lim_{i \to +\infty} \abs{Y^i} = 0
	\]
	(o se mantiene acotado).
\end{defi}

\begin{defi}[factor!de amplificación]
	Dado método, lo reescribimos para que quede de la forma
	\[
		Y^{i+1} = Y^i G
	\]
	Tomando valores absolutos, obtenemos
	\[
		\abs{Y^{i+1}} = \abs{Y^i} \abs{G}
	\]
	Llamaremos a $\abs{G}$ el factor de amplificación.
\end{defi}

\begin{obs}
	Un método es estable si su factor de amplificación es $\leq 1$.
\end{obs}

\begin{defi}[región de estabilidad]
	Llamamos región de estabilidad a la región del plano $\lambda h$ en la
	cual un método es estable.
\end{defi}

\begin{defi}[método!incondicionalmente estable]
	Diremos que un método es incondicionalmente estable si la región de
	estabilidad incluye
	\[
		\left\{ \lambda h \mid \text{Re}(\lambda h) \leq 0 \right\}
	\]
\end{defi}

Analizaremos ahora la estabilidad de los métodos que hemos trabajado.

\subsection{Método de Euler}
Recordemos el método de Euler
\[
	Y^{i+1} = Y^i = hf\left( x_i, y^i \right)
\]
Reescribiendo
\[
	Y^{i+1} = Y^i + \lambda h Y^i = Y^i(1 + \lambda h)
\]
de donde se decide que el factor de amplificación es $\abs{1 + \lambda h}$ y por
lo tanto el método es estable si $\abs{1 + \lambda h} \leq 1$. En caso de que
$\lambda \in \real$, la condición de estabilidad absoluta es $-2 \leq \lambda h
\leq 0$. Por lo tanto, la región de estabilidad es

\begin{figure}[h]
	\centering
	\begin{tikzpicture}
		\begin{axis}[
			%ticks=none,
			axis lines=middle,
			ymin=-1, ymax=1,
			xmin=-3, xmax=3,
			ylabel={$\text{Im}(\lambda h)$},
			xlabel={$\text{Re}(\lambda h)$},
			axis equal
		]
			\draw[blue,fill=blue,opacity=0.5] (axis cs: -1,0) circle (1);
			\draw[thick] (axis cs: -1,0) circle (1);
		\end{axis}
	\end{tikzpicture}
\end{figure}

\subsection{Método de Euler hacia atrás}
Recordemos primero el método:
\[
	Y^{i+1} = Y^i + hf\left( x_{i+1}, Y^{i+1} \right)
\]
De aquí se deduce que
\[
	Y^{i+1} = Y^i + \lambda h Y^{i+1} \implies Y^i = Y^{i+1} (1 - \lambda
	h) \implies \abs{Y^{i+1}} = \abs{Y^i} \frac{1}{\abs{1 - \lambda h}}
\]
Y el factor de amplificación es $\frac{1}{\abs{1-\lambda h}}$ y el método es
estable si $\abs{1 - \lambda h} \geq 1$. En el caso $\lambda \in
\real$, el esquema es absolutamente estable para $\left\{ \lambda h < 0 \right\}
\cap \left\{ \lambda h > 2 \right\}$.
\begin{figure}[h]
	\centering
        \resizebox{0.45\textwidth}{!}{
            \begin{tikzpicture}
                \begin{axis}[
                        axis lines=middle,
                        ymin=-2, ymax=2,
                        xmin=-2, xmax=4,
                        ylabel={$\text{Im}(\lambda h)$},
                        xlabel={$\text{Re}(\lambda h)$},
                        axis equal
                    ]
                    \clip (axis cs:1,0) circle (1) [reverseclip];
                    \draw[fill=blue,opacity=0.5] (axis cs: 1,0) ellipse (3 and 2);
                    \draw[thick] (axis cs:1,0) circle (1);
                \end{axis}
            \end{tikzpicture}
        }
    \end{figure}

    En particular, el método de Euler hacia atrás  es absolutamente estable para
    cualquier $h > 0$ si $\lambda < 0$.

    \subsection{Método de las diferencias centradas}
    Primero, recordemos el método de las diferencias centradas:
    \[
        Y^{i+1} = Y^{i-1} + 2 h f\left( x_i, Y^i \right) \implies Y^{i+1} =
        Y^{i-1} + 2 \lambda h Y^i
    \]
    Podemos reescribir esta ecuación de la siguiente forma matricial
    \[
        \begin{pmatrix}
            Y^{i+1} \\ Y^i
        \end{pmatrix}
        = \underbrace{
            \begin{pmatrix}
                2\lambda h & 1 \\
                1 & 0
            \end{pmatrix}
        }_{G}
        \begin{pmatrix}
            Y^i \\ Y^{i-1}
        \end{pmatrix}
    \]
    Por lo tanto es estable si $\abs{\lambda_1}, \abs{\lambda_2} \leq 1$, donde
    $\lambda_1, \lambda_2$ son los VAPs de $G$.

    Busquemos ahora los VAPs de $G$, primero calculamos el polinoio característico
    \[
        Q_G(t) = t^2 - 2\lambda ht -1
    \]
    Ahora buscamos sus raíces
    \[
        Q_G(t) = 0 \implies t = \lambda h \pm \sqrt{\lambda^2 h^2 +1} \implies
        \begin{cases}
            \lambda_1 = \lambda h + \sqrt{\lambda^2h^2+1} \\
            \lambda_2 = \lambda h - \sqrt{\lambda^2h^2+1}
        \end{cases}
    \]
    De donde sigue que la condición de estabilidad absoluta es $\abs{\lambda h \pm
    \sqrt{\lambda^2h^2+1}} < 1$. Y la región de estabilidad es:

    \begin{figure}[h]
        \centering
        \begin{tikzpicture}
            \begin{axis}[
                    axis lines=middle,
                    ymin=-2, ymax=2,
                    xmin=-2, xmax=4,
                    ylabel={$\text{Im}(\lambda h)$},
                    xlabel={$\text{Re}(\lambda h)$},
                    axis equal
                ]
                \draw[ultra thick, blue] (axis cs: 0,-1) -- (axis cs:0,1);
            \end{axis}
        \end{tikzpicture}
    \end{figure}
    En particular, si $\lambda \in \real$, el método es inestable.

    \section{Métodos de Runge-kutta}

    \subsection{Método de Heun}
    El método de Heun está basado en una aproximación centrada,$\left( T_i(h)
    =\O\left( h^2 \right)\right)$. Considerando el polinomio de Taylor centrado en
    $x_{i + \frac{1}{2}} = x_i + \frac{h}{2}$ y evaluando en $x = x_i$,
    $x=x_{i+1}$, obtenemos
    \[
        y^{i+1} = y^i + hf\left( x_{i + \frac{1}{2}}, y^{i+1} \right) + \O\left(
        h^2 \right)
    \]
    Y por otro lado
    \[
        hf\left( x_{i+\frac{1}{2}}, y^{i+1} \right) = \frac{1}{2} \left( f\left(
        x_i, y_i \right) + f\left( x_{i+1}, y^{i+1} \right) \right) +
        \O\left( h^2 \right)
    \]
    Así pues, negligiendo los errores, el método de Heun es
    \[
        Y^{i+1} = Y^i + \frac{h}{2}\left( f\left( x_i, y^i \right) + f\left(
        x_{i+1}, y^{i+1} \right) \right)
    \]
    que tiene orden 2.

    Observamos sin embargo que el método de Heun es implícito, para solucionar este
    ``problema'', empleamos la siguiente técnica de dos pasos
    \[
        \begin{cases}
            \prescript{*}{}{Y^{i+1}} = Y^i + hf\left( x_i, Y^i \right) \\
            Y^{i+1} = Y^i + \frac{h}{2}\left( f\left( x_i, Y^i \right) +
            f\left( x_{i+1}, \prescript{*}{}{Y^{i+1}} \right) \right)
        \end{cases}
    \]
    Es decir, primero hacemos una predicción de $Y^{i+1}$ con el método de Euler, y
    luego la utilizamos para calcular $Y^{i+1}$. Usando esta técnica mantenemos el
    orden 2 y hacemos el método explícito.

    Si utilizamos la notación habitual en los métodos de Runge-Kutta:
    \[
        \begin{cases}
            k_1 = f\left( x_i, Y^i \right) \quad k_2 = f\left( x_i + h, Y^i
            + hk_1 \right) \\
            Y^{i+1} = Y^i + h\left( \frac{1}{2} k_1 + \frac{1}{2}k_2 \right)
        \end{cases}
    \]

    \subsection{Métodos de Runge-Kutta}

    En general, los métodos RK son de la forma
    \[
        \begin{cases}
            Y^{i+1} = Y^i + h\left( b_1k_1 + \cdots + b_sk_s \right) \\
            k_1 = f\left( x_i + c_1 h, Y^i + h\left( a_{11}k_1 + \cdots +
            a_{1s}k_s \right) \right) \\
            \vdots \\
            k_s = f\left( x_i + c_sh, Y^i + h\left( a_{s1}k_1 + \cdots +
            a_{ss} k_s \right) \right)
        \end{cases}
    \]

    \begin{defi}[tabla de Butcher]
        Para simplificar la escritura de los métodos RK, podemos emplear la siguiente
        tabla, cconocida como la tabla de Butcher.
        \[
            \begin{tabular}{c|ccc}
                $c_1$ & $a_{11}$ & $\cdots$ & $a_{1s}$ \\
                $\vdots$ & $\vdots$ & & $\vdots$ \\
                $c_s$ & $a_{s1}$ & $\cdots$ & $a_{ss}$ \\ \hline
                & $b_1$ & $\cdots$ & $b_s$
            \end{tabular}
        \]
    \end{defi}
    Donde el número de filas nos marca el número de etapas. Si la matríz de
    coeficientes $a$ solo tiene valores por debajo de la diagonal, el método es
    explícito. Si tambien tiene valores en la diagonal (pero no encima de la
    diagonal), diremos que el método es diagonalmente implícito.

    \begin{example}
        La tabla de Butcher para el método de Heun es
        \[
            \begin{tabular}{c|ccc}
                0 & 0 & 0 \\
                1 & 1 & 0 \\ \hline
                & $\frac{1}{2}$ & $\frac{1}{2}$
            \end{tabular}
        \]
        Como se puede observar, tiene dos etapas.
    \end{example}

    El error global de estos métodos es
    \[
        E_m \sim C m^{-q}, \, E_m \sim kh^q
    \]
    Es decir, tienen orden $q$. ($m$ es el número de pasos y $C$ y $k$ son
    constantes).

    Los RK más populares son los explícitos que con $s$ etapas (coste de $s$
    evaluaciones de $f$) tienen orden $q = s$. Esto solo es ``posible'' para $s \leq
    4$.

    Es interesante, destacar de entre estos métodos el método RKF45 (\texttt{ode45}
    en matlab y octave) cuyo funcionamiento se puede consultar en la wikipedia.

    \begin{example}
        Ejercicio 2 del final de 2018.

        Se quiere resolver un problema de transporte
        \begin{equation}\label{eq:primerafinal}
            au^\prime - \nu u^{\prime\prime} = 1 \quad x \in \left( 0, 1
            \right)
        \end{equation}
        con la velocidad de convección $a=1$ y coeficiente de difusión $\nu=1$.
        En una primera fase, se plantea la resolución de un problema de valor
        inicial, con las condiciones iniciales
        \begin{equation}\label{eq:finalconditions}
            u(0) = 0, \quad u^\prime(0) = \frac{1}{2}
        \end{equation}
        \begin{enumerate}[a)]
        \item Reescribe el problema \ref{eq:primerafinal} con las
            condiciones iniciales \ref{eq:finalconditions} como un
            sistema de ecuaciones diferenciales ordinarias de primer
            orden.
        \item \label{item:dosfinal} Calcula una aproximación de la solución en $x=1$ mediante
            $m=8$ pasos del siguiente método de Runge-Kutta de
            segundo orden:
            \[
                Y^{i+1} = Y^i + \frac{h}{2}\left( k_1 + k_2
                \right) \text{ con }
                \begin{cases}
                    k_1 = f\left( x_i, Y^i \right) \\
                    k_2 = f\left( x_i + h, Y^i + hk_1 \right)
                \end{cases}
            \]
        \item La solución del problema planteado en \ref{item:dosfinal}
            es $u(1) \approx 0.140859$. Calcula el error de la
            aproximación obtenida en el apardato anterior. ¿Cuántas
            cifras significativas correctas tiene la aproximación?
            Estima el número de pasos necesarios para obtener una
            aproximación de la solución con 5 cifras signiicativas
            correctas. Explica cómo has obtenido este resultado.
    \end{enumerate}
    En una segunda fase interesa resolver la ecuacion \ref{eq:primerafinal}
    con las condiciones de contorno
    \[
        u(0) = 0, \quad u(1) = 0
    \]
    \begin{enumerate}[a)]\setcounter{enumi}{3}
    \item Propón un procedimiento para resolver el problema de
        contorno utilizando métodos de resolución de EDOs.
        Detalla los problemas que hay que resolver y propón
        métodos para resolverlos
    \item Implementa el procedimiento propuesto en el apartado
        anterior y di qué método(s) numéricos has empleado en la
        resolución. Indica, si es necesario, la discretización o
        tolerancias tomadas. ¿Qué condiciones iniciales
        proporcionan la solución del problema de contorno
        planteado?
\end{enumerate}

Solución

\begin{enumerate}[a)]
\item Queremos reescribir el problema de la forma
    \[
        \begin{cases}
            \dv{\bar{y}}{x} = \bar{f}\left( x, \bar{y}
            \right) \\
            \bar{y}(0) = \bar{\alpha}
        \end{cases}
    \]
    Para ello, tomamos $\bar{y} = \begin{pmatrix} u \\
    u^\prime \end{pmatrix}$ y calculamos:
    \[
        \begin{cases}
            \pdv{\bar{y}_1}{x} = u^\prime =
            \bar{y}_2 \\
            \pdv{\bar{y}_2}{x} = u^{\prime\prime} =
            u^\prime -1 = \bar{y}_2 -1
        \end{cases}
    \]
    De donde obtenemos
    \[
        \bar{f}\left( x, \bar{y} \right) =
        \begin{pmatrix}
            \bar{y}_2 \\ \bar{y}_2 - 1
        \end{pmatrix}
        \text { o }
        \bar{f}\left( x, \bar{y} \right) =
        \begin{pmatrix}
            0 & 1 \\ 0 & 1
        \end{pmatrix}
        \bar{y} +
        \begin{pmatrix}
            0 \\ -1
        \end{pmatrix}
    \]
    Y tan solo queda especificar $\bar{\alpha} =
    \begin{pmatrix} 0 \\ 1/2 \end{pmatrix}$.
\item Solución en matlab.
\item La aproximación tiene una cifra significativa correcta
    (una mera comprobación). Para obtener 5 cifras
    significativas corretas, necesitamos $r_m \simeq 0.5
    \cdot 10^{-5}$. Sabemos que
    \[
        r_8 = 0.023 = \frac{C}{m^2} =
        \frac{C}{64}
    \]
    de donde obtenemos $C = 1.472$ y ahora
    \[
        r_m = 0.5 \cdot 10^{-5} = \frac{C}{m^2} =
        \frac{1.472}{m^2} \implies m =
        \sqrt{\frac{1.472}{0.5 \cdot 10^{-5}}} \approx
        542.58
    \]
    por lo tanto necesitaríamos unos 543 pasos.
\item Para resolver el problema de contorno, tomaremos la
    función $u^\beta$ definida como $u^\beta = \bar{y}_1$ con
    \[
        \begin{cases}
            \dv{\bar{y}}{x} =
            \begin{pmatrix}
                0 & 1 \\
                0 & 1
            \end{pmatrix}
            \bar{y} +
            \begin{pmatrix}
                0 \\ -1
            \end{pmatrix}
            \\
            \bar{y}(0) =
            \begin{pmatrix}
                0 \\ \beta
            \end{pmatrix}
        \end{cases}
    \]
    Y encontraremos $\beta$ tal que se cumpla la condición
    en $x=1$. Ahora tan solo hace falta buscar el cero de la
    función
    \[
        F(\beta) = u^\beta(1) - 0 = ^\beta Y^m_{(1)} - 0
    \]
    para ello podemos utilizar la función de matlab
    \texttt{fzero(F, 0)}.
\item Solución en matlab.
        \end{enumerate}
    \end{example}
