\chapter{Derivación. Funciones holomorfas}
\section{Funciones holomorfas}
\begin{defi}[derivada compleja]
    Sea $U \subseteq \cx$ un abierto y sea $f \colon U \to \cx$ una función.
    Decimos que $f$ es derivable en $z_0 \in U$ si existe el límite
    \[
        \lim_{z \to z_0} \frac{f(z) - f\left( z_0 \right)}{z - z_0} =
        \lim_{h \to 0} \frac{f\left( z_0 + h \right) - f\left( z_0 \right)}{h}
    \]
    Si existe el límite lo notaremos como $f^\prime\left( z_0 \right)$ y lo
    denominaremos derivada de $f$ en $z_0$.
\end{defi}
\begin{prop}
    Sea $f \colon U \subseteq \cx \to \cx$ una función derivable en $z_0 \in U$,
    entonces $f$ es continua en $z_0$.
\end{prop}
\begin{prop}
    Sean $f, g \colon U \to \cx$ dos funciones derivables en el punto $z \in U$
    entonces
    \begin{enumerate}[i)]
        \item $(f+g)^\prime(z) = f^\prime(z) + g^\prime(z)$ y $(f-g)^\prime(z) =
            f^\prime(z) - g^\prime(z)$
        \item $(fg)^\prime(z) = f^\prime(z)g(z) + f(z)g^\prime(z)$
        \item $\left( \frac{f}{g} \right)^\prime(z) = \frac{f^\prime(z)g(z) -
            f(z)g^\prime(z)}{g(z)^2}$ si $g(z) \neq 0$
        \item Si $g$ es derivable en $f(z)$ entonces $(g \circ f)^\prime(z) =
            g^\prime\left( f(z) \right) f^\prime(z)$
    \end{enumerate}
\end{prop}
\begin{defi}[función!holomorfa]\idx{función!derivada}
    Sea $U \subseteq \cx$ un abierto, decimos que una función $f \colon U \to
    \cx$ es holoforma sobre $U$ si para todo $z \in U$, $f$ es derivable en $z$.
    En este caso, la función
    \[
        \begin{aligned}
            f^\prime \colon U &\to \cx \\
            z &\mapsto f^\prime(z)
        \end{aligned}
    \]
    se llama función derivada.
\end{defi}
\begin{defi}[función!primitiva]
    Si $F \colon U \subseteq \cx \to \cx$ es una  función holomorfa con derivada
    $f$, decimos que $F$ es una primitiva (o antiderivada) de $f$.
\end{defi}
\begin{example}
    \begin{itemize}
        \item[]
        \item Las constantes son holomorfas en $\cx$ con derivada 0.
        \item La función identidad es holomorfa en $\cx$ con derivada 1.
        \item Los polinomios $\sum^n_{k=0}a_kz^k$ son holomorfos en todo $\cx$
            con derivada $\sum^n_{k=1}ka_kz^{k-1}$.
        \item Las funciones racionales $f(z) = \frac{r(z)}{s(z)}$ son holomorfas
            en $\cx \setminus \left\{ \text{ceros de } s(z) \right\}$.
        \item La función $f(z) = \overline{z}$ no es derivable en ningún punto
            de $\cx$
            \[
                \lim_{h \to 0} \frac{f(z+h)-f(z)}{h} = \lim_{h \to 0}
                \frac{\overline{z}+\overline{h}-\overline{z}}{h} =
                \lim_{h \to 0} \frac{\overline{h}}{h}
            \]
            que evidentemente no está definido y concluimos que $\overline{z}$
            no es holomorfa en ningún abierto de $\cx$.
        \item La función $f(z) = \abs{z}^2 = z\overline{z}$ solo es derivable en
            $z = 0$:
            \[
                \lim_{h \to 0} \frac{f(0+h) - f(0)}{h} = \lim_{h \to 0}
                \frac{h\overline{h} - 0}{h} = 0
            \]
            sin embargo
            \[
                \lim_{h \to 0} \frac{f(z+h) - f(z)}{h} = \lim_{h \to
                0}\frac{(z+h)\left( \overline{z} + \overline{h} \right) -
                z\overline{z}}{h} = \lim_{h \to 0} \frac{h\overline{h} +
                2\re\left( z \overline{h} \right)}{h} 
            \]
            Reescribiremos el límite de la siguiente forma, $z = a +ib$ y $h =
            x+iy$.
            \[
                \lim_{h \to 0} \frac{h\overline{h} + 2\re\left( z \overline{h}
                \right)}{h} = 2 \lim_{h \to 0} \frac{\re\left( z \overline{h}
                \right)}{h} = \lim_{(x, y) \to (0,0)} \frac{2ax + 2by}{x+iy}
            \]
            Si nos aproximamos por el eje real $y = 0$, obtenemos como resultado
            del límite $2a$ y si nos aproximamos por el eje imaginario $x = 0$,
            obtenemos $\frac{2b}{i}$. Por lo tanto, el límite solo está definido
            si $a = b = 0$.
    \end{itemize}
\end{example}
\begin{prop}
    Si $f(z) = \sum a_n \left( z-z_0 \right)^n$ para algún $z \in \text{D}\left(
    z_0; R \right)$ para algún $R > 0$. Entonces $f$ es derivable en
    $\text{D}\left( z_0; R \right)$ y su derivada viene dada por la serie de
    potencias $f^\prime(z) = \sum_{n \geq 1} na_n\left( z - z_0
    \right)^{n-1}$
\end{prop}
\begin{proof}
    Supondremos $z_0 = 0$. Queremos demostrar que $f'\left( z_1 \right) =
    \sum_{n \geq 1} n a_nz^{n-1}$. Sea $S_n(z) = \sum^n_{k=0} a_nz^n$, $R_n =
    \sum^\infty_{k=n+1}a_nz^n = f(z) - S_n(z)$. Los polinomios son derivables,
    por lo tanto $S^\prime_n (z) = \sum^n_{k=1}ka_kz^{k-1}$. Definiremos por
    último $g(z) = \sum^\infty_{k=1} ka_kz^{k-1}$ y queremos ver que $f'(z) =
    g(z)$, se tiene que
    \[
        \begin{gathered}
            \lim_{z \to z_1} \left( \frac{f(z) - f\left( z_1 \right)}{z - z_1} -
            g\left( z_1 \right)\right) = 0 \iff\\\iff \left[ \forall \varepsilon >0
            \exists \delta > 0 \tq  \abs{z - z_1} < \delta \implies
            \abs{\frac{f(z) - f\left( z_1 \right)}{z - z_1} - g\left( z_1
            \right)} < \varepsilon \right]
        \end{gathered}
    \]
    Por otro lado
    \begin{equation}\label{eq:cosa}
        \begin{gathered}
            \abs{\frac{f(z) -f\left( z_1 \right)}{z - z_1} - g\left( z_1
            \right)} =\\=\abs{\frac{S_n(z) - S_n\left( z_1 \right)}{z-z_1} -
            S^\prime_n\left( z_1 \right) +S^\prime_n\left( z_1 \right) - g\left(
        z_1 \right) + \frac{R_n(z) - R_n\left( z_1 \right)}{z - z_1}} \leq
            \\ \leq \abs{\frac{S_n(z) - S_n\left( z_1 \right)}{z - z_1} -
            S^\prime_n\left( z_1 \right)} + \abs{S^\prime_n\left( z_1
            \right) - g\left( z_1 \right)} + \abs{\frac{R_n(z) - R_n\left( z_1
            \right)}{z-z_1}}
        \end{gathered}
    \end{equation}
    Ahora, acotaremos cada uno de los términos por $\frac{\varepsilon}{3}$

    Tenemos
    \[
        \begin{gathered}
            \abs{\frac{a_kz^k-a_kz^k_1}{z-z_1}} = \abs{a_k}
            \abs{z^{k-1}+z^{k-2}z_1+ \cdots + z z^{k-2}_1 + z^{k-1}_1} \leq
            \\\leq \abs{a_k}\left( \abs{z}^{k-1} + \abs{z}^{k-2} \abs{z_1} +
            \cdots \abs{z_1}^{k-1} \right) \leq k \abs{a_k}r^{k-1}
        \end{gathered}
    \]
    Ya que $z_1 \in \text{D}\left( 0; R \right) \implies \exists r \in
    \real^\ast$ tal que $\abs{z_1} < r < R$ y $z \in \text{D}\left( 0; r
    \right)$. Por lo tanto
    \[
        \abs{\frac{R_n(z) -R_n\left( z_1 \right)}{z - z_1}} =
        \abs{\sum^\infty_{k=n+1} \frac{a_kz^k - a_kz^k_1}{z-z_1}} \leq
        \sum^\infty_{k=n+1}k \abs{a_k}r^{k-1}
    \]
    Como $\sum^\infty_{k=1}k \abs{a_k}r^{k-1}$ es convergente, $\exists N_1$ tal
    que $\forall n \geq N_1$ $\abs{\frac{R_n(z) - R_n\left( z_1 \right)}{z-z_!}}
    < \frac{\varepsilon}{3}$.

    También se tiene que $\abs{S^\prime_n\left( z_1 \right) - g\left( z_1
    \right)} < \frac{\varepsilon}{3}$ si $n \geq N_2$ porque $S^\prime_n\left(
    z_1 \right) \to g\left( z_1 \right)$.

    Por último sabemos que (fijado $n$) $\exists \delta_n$ tal que
    $\abs{\frac{S_n(z) - S_n\left( z_1 \right)}{z-z_1} - S^\prime_n\left( z_1
    \right)} < \frac{\varepsilon}{3}$ si $\abs{z-z_1} < \delta_n$ ya que
    $S_n(z)$ es derivable en $z_1$.

    Por tanto, tomamos $n \geq \max\left\{ N_1, N_2 \right\}$ y tomamos $\delta
    \leq r - \abs{z_1}$, se tiene que
    \[
        0 < \abs{z - z_1} < \delta \implies \abs{z} \leq \abs{z} \leq
        \abs{z-z_1} + \abs{z_1} < \delta + \abs{z_1} \leq r
    \]
    Y concluimos por tanto que $\text{\ref{eq:cosa}} \leq \frac{\varepsilon}{3} +
    \frac{\varepsilon}{3} + \frac{\varepsilon}{3}$ si $\abs{z - z_1} < \delta$.
\end{proof}
\begin{obs}
    Las series $\sum a_n \left( z - z_0 \right)^n$ y $\sum n a_n \left( z - z_0
    \right)^{n-1}$ tienen el mismo radio de convergencia.
\end{obs}
\begin{col}
    Toda función analítica es derivable y su derivada también es analítica. Por
    lo tanto, es infinitamente derivable.
\end{col}
\begin{example}
    \begin{itemize}
        \item[]
        \item $e^z$ es derivable y $\left( e^z \right)^\prime = e^z$.
        \item $\cos z$ es derivable y $\left( \cos z \right)^\prime =-\sin z$.
        \item $\sin z$ es derivable y $\left( \sin z \right)^\prime = \cos z$
    \end{itemize}
\end{example}
\section{Ecuaciones de Cauchy-Riemman}
\begin{obs}
    Sea $f \colon U \subseteq \cx \to \cx$, podemos considerar $f = u + iv$ y por
    otro lado podemos considerar $f$ como una función $\real^2$ obteniendo las
    siguiente identidades
    \[
        \begin{aligned}
            \pdv{f}{x}\left( z_0 \right) = \lim_{t \to 0} \frac{f\left( z_0 + t
            \right) - f\left( z_0 \right)}{t} = \pdv{u}{x}\left( z_0 \right) + i
            \pdv{v}{x}\left( z_0 \right) \\
            \pdv{f}{y}\left( z_0 \right) = \lim_{t \to 0} \frac{f\left( z_0 + it
            \right) - f\left( z_0 \right)}{t} = \pdv{u}{y}\left( z_0 \right) + i
            \pdv{v}{y}\left( z_0 \right)
        \end{aligned}
    \]
    Y por último tenemos
    \[
        \left( \text{J}f \right) \left( z_0 \right) =
        \begin{pmatrix}
            \pdv{u}{x}\left( z_0 \right) & \pdv{u}{y}\left( z_0 \right) \\
            \pdv{v}{x}\left( z_0 \right) & \pdv{v}{y}\left( z_0 \right)
        \end{pmatrix}
    \]

    Además, notamos que existe una similitud entre la definición de diferencial
    en $\real^2$, $\text{D}f\left( z_0 \right)$ tal que
    \[
        \lim_{h \to (0,0)} \frac{\norm{f\left( z_0 + h \right) - f\left( z_0
        \right) - \left( \text{D}f \right)\left( z_0 \right)h}}{\norm{h}} = 0
    \]
    y la definición de derivada compleja, $f'\left( z_0 \right)$ tal que
    \[
        \lim_{h \to 0} \frac{\abs{f\left( z_0 + h \right) - f\left( z_0 \right) -
        f'\left( z_0 \right)h}}{\abs{h}} = 0
    \]
    Ya que $\lim g(z) = 0 \iff \lim \abs{g(z)} = 0$.
\end{obs}
\begin{obs}
    Sea $L \colon \cx \to \cx$ una aplicación $\real$-lineal con matriz $
    \begin{bmatrix}
        a & c \\ b & d
    \end{bmatrix}
    $ con $a, b, c, d \in \real$. Entonces $L$ es $\cx$-lineal si y solo si
    $L(z) = wz$, es decir, si y solo si $d = a$ y $c = -b$ ($w = a+bi$)
\end{obs}
\begin{teo*}
    Sea $U \subseteq \cx$ un abierto y $f \colon U \to \cx$ una función.
    Entonces $f$ es derivable en $z_0 \in U$ $\iff$ $f$ es diferenciable en
    $z_0$ como función $U \subseteq \real^2 \to \real^2$ y su diferencial es
    $\cx$-lineal.

    En ese caso, la derivada de $f$ es
    \[
        f'\left( z_0 \right) = \pdv{f}{x}\left( z_0 \right) = \pdv{u}{x}\left(
        z_0 \right) + i \pdv{v}{x}\left( z_0 \right) = -i \pdv{f}{y}\left( z_0
        \right) = \pdv{v}{y}\left( z_0 \right) - i \pdv{u}{y}\left( z_0 \right)
    \]
\end{teo*}
\begin{proof}
    Primero demostraremos la implicación hacia la derecha. Sea $\left( \text{D}f
    \right)\left( z_0 \right)(h) = f'\left( z_0 \right)h$ que es una aplicación
    $\cx$-lineal y cumple que
    \[
        \lim_{h \to 0} \frac{\norm{f\left( z_0+h \right)-f\left( z_0 \right) -
        f'\left( z_0 \right)h}}{\norm{h}} = 0
    \]

    Veamos ahora la implicación inversa. Sabemos que $\left( \text{D}f
    \right)\left( z_0 \right)(h) = wh$ para algún $w \in \cx$ definimos ahora
    $f'\left( z_0 \right) = w$ y se tiene que
    \[
        \lim_{h \to 0} \frac{\abs{f\left( z_0 +h \right) - f\left( z_0 \right) -
        f'\left( z_0 \right)h}}{\abs{h}} = 0
    \]

    La última igualdad sigue inmediatamente de la última observación y del hecho
    de que la matriz de $\left( \text{D}f \right)\left( z_0 \right)$ es
    $\left( \text{J}f \right)\left( z_0 \right)$.
\end{proof}
\begin{defi}[ecuaciones de Cauchy-Riemman]\label{prop:eq-c-r}
    La ecuaciones de Cauchy-Riemman relacionan las derivadas parciales de dos
    funciones $u,v \colon \real^2 \to V$ en el punto $z_0 \in \real^2$ de
    la siguiente forma
    \[
        \begin{cases}
            \pdv{u}{x}\left( z_0 \right) = \pdv{v}{y}\left( z_0 \right) \\
            \pdv{u}{y}\left( z_0 \right) = -\pdv{v}{x}\left( z_0 \right)
        \end{cases}
    \]
\end{defi}
\begin{col}
    $f$ es diferenciable en $z_0 \iff f$ es derivable en $z_0$ y satisface
    las \hyperref[prop:eq-c-r]{ecuaciones de Cauchy-Riemman}
\end{col}
\begin{example}
    \begin{itemize}
        \item[]
        \item $e^z = e^x\cos y + i e^x \sin y$ y se tiene
            \[
                \begin{cases}
                    \pdv{u}{x} = e^x \cos y = \pdv{v}{y} \\
                    \pdv{u}{y} =-e^x \sin y =-\pdv{v}{x}
                \end{cases}
            \]
        \item $z^2 = (x + iy)^2 = x^2 - y^2 + 2xyi$ y
            \[
                \begin{cases}
                    \pdv{u}{x} = 2x = \pdv{v}{y} \\
                    \pdv{u}{y} = 2y =-\pdv{v}{x}
                \end{cases}
            \]
        \item $f(z) = 2 \abs{z}^2 - \overline{z}^2 = 2\left( x^2 + y^2 \right) -
            \left( x^2-y^2-2ixy \right) = x^2 + 3x^2 + 2xyi$ y
            \[
                \begin{cases}
                    \pdv{u}{x} = 2x = \pdv{v}{y} \\
                    \pdv{v}{y} = 6y = -2y = - \pdv{v}{x} \iff y = 0
                \end{cases}
            \]
    \end{itemize}
\end{example}
