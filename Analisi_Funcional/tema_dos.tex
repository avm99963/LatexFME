\chapter{Espais de Hilbert}

\section{Propietats generals}

\begin{defi}[producte escalar]
	Sigui $H$ un espai vectorial real. Direm que una forma bilineal simètrica 
	$(\cdot,\cdot) \colon H \times H \to \real$ és un producte escalar si $(u,u) \geq 0$
	$\forall u \in H$ i $(u,u) = 0 \iff u = 0$.
\end{defi}

\begin{lema}
	\begin{enumerate}[i)]
		\item[]
		\item Desigualtat de Cauchy-Schwarz: $|(u,v)| \leq |(u,u)|^{\frac{1}{2}}|(v,v)|^{\frac{1}{2}}$,
		$\forall u,v \in H$.
		\item $|u| = (u,u)^{\frac{1}{2}}$ és una norma en $H$.
		\item Identitat del para\l.lelogram: $|u+v|^2 + |u-v|^2 = 2|u|^2 + 2|v|^2$.
	\end{enumerate}
\end{lema}

\begin{proof}
	\begin{enumerate}[i)]
	\item[]
	\item Donat $t\in \real$, com que el producte escalar és definit positiu, se satisfà que $(u+tv,u+tv) \geq 0$.
	Per binealitat i simetria, es té $t^2 (v,v) + 2t(u,v) + (u,u) \geq 0$. Aquesta expressió és una equació de
	segon grau sobre $t$ sense arrels reals o amb una arrel doble, i per tant el seu discriminant és no positiu.
	En altres paraules:
	\[
		4(u,v)^2 - 4(u,v)(u,v) \leq 0 \implies |(u,v)| \leq |(u,u)|^{\frac{1}{2}}|(v,v)|^{\frac{1}{2}}.
	\]
	\item L'única propietat que no és immediata és la desigualtat triangular, que s'obté usant la
	desigualtat de Cauchy-Schwarz:
	\[
		|u+v|^2 = (u+v,u+v) = (u,u) + 2(u,v) + (v,v) \leq |u|^2 + 2|u||v| + |v|^2 = (|u|+|v|)^2.
	\]
	\item Es desenvolupa la suma amb la definició de la norma i les propietats del producte escalar.
	Únicament cal notar que 
	\[
		\begin{rcases}
			(u+v,u+v) = (u,u)+2(u,v)+(v,v) \\
			(u-v,u-v) = (u,u)-2(u,v)+(v,v)
		\end{rcases}
		\implies |u+v|^2 + |u-v|^2 = 2(u,u) + 2(v,v).
	\]
	\end{enumerate}
\end{proof}

\begin{defi}[espai!de Hilbert]
	Un espai de Hilbert $H$ és un espai vectorial real dotat d'un producte escalar, satisfent que
	$H$ sigui complet respecte la norma induïda $|\cdot|$.
\end{defi}

\begin{example}
	\begin{itemize}
		\item[]
		\item L'espai $\ell^2$ de les successions de quadrat integrable és de Hilbert amb el producte escalar:
		\[
			(u,v) := \sum_{k \geq 0} u_k v_k.
		\]
		\item Més en general, l'espai de funcions $L^2 (\Omega)$ és de Hilbert amb el producte escalar:
		\[
			(u,v) := \int_{\Omega} u(x) v(x) d\mu.
		\]
		L'aplicació anterior està ben definida per la desigualtat de Cauchy-Schwarz:
		\[
			|(u,v)| \leq \| u\|_{L^2} \| v\|_{L^2} < \infty.
		\]
		És senzill veure que és, en efecte, un producte escalar, i que la seva norma induïda és la que ja
		havíem estudiat. En particular, $L^2$ amb la norma induïda és de Banach, i per tant és de Hilbert.
		\item L'espai de Sobolev d'ordre 1 en $\Omega \subset \real^n$, definit per:
		\[
			H^1(\Omega) := \left\{u \in L^2(\Omega), \; \partial_{x_i} u \in L^2(\Omega), \; 1 \leq i \leq n \right\},
		\]
		és de Hilbert amb el producte escalar:
		\[
			(u,v)_{1,\Omega} := \int_{\Omega} ( uv + \sum_{i=1}^n \partial_{x_i} u \, \partial_{x_i} v ) d \mu.
		\]
		De fet, l'espai $H^m(\Omega)$, corresponent a les funcions tals que totes les seves derivades parcials
		fins a ordre $m$ són de $L^2(\Omega)$, és de Hilbert amb el producte escalar anàleg a l'anterior:
		\[
			(u,v)_{m,\Omega} := \sum_{|\alpha| \leq m}\int_{\Omega} (D^{\alpha}u \, D^{\alpha}v) d \mu.
		\]
	\end{itemize}
\end{example}

\begin{teo}[Teorema de la projecció sobre un convex tancat]
	Sigui $H$ un espai de Hilbert, i $K \subset H$ un subconjunt convex tancat i no buit. Llavors, per a tot $f \in H$
	existeix un únic $u \in K$ tal que:
	\[
		|f-u| = \min_{v \in K} \{ |f-v| \}
	\]
	A més, $u$ es caracteritza per les dues propietats següents:
	\begin{enumerate}[i)]
		\item $u \in K$. \label{item:projconvi}
		\item $(f-u,v-u) \leq 0$, $\forall v \in K$. \label{item:projconvii}
	\end{enumerate}
	S'escriu $P_K f := u$. L'aplicació $P_K$ és contínua de $H$ en $H$, i satisfà $|P_K f_1 - P_K f_2| \leq |f_1-f_2|$.
\end{teo}

\begin{proof}
	Demostrarem primer l'existència de $u$. Considerem una successió $(v_n)$, amb $v_n \in K$ i tal que
	\[
		d_n := |f-v_n| \to d = \inf_{v \in K} |f-v|.
	\]
	Demostrarem que la successió és de Cauchy, i per tant convergent en $K$. En efecte, aplicant la idenitat
	del para\l.lelogram a $f-v_n$ i $f-v_m$, obtenim:
	\[
		|2f - (v_n + v_m)|^2 + |v_m - v_n|^2 = 2|f-v_n|^2 + 2|f-v_m|^2 = 2(d_n^2 + d_m^2).
	\]
	Ara bé, com que $\frac{v_n+v_m}{2} \in K$ (per convexitat), es té $|f - \frac{v_n+v_m}{2}| \geq d$, i per tant:
	\[
		|v_m-v_n|^2 \leq  2(d_n^2 + d_m^2) - 4d^2 \to 0.
	\]
	Així, $(v_n)$ convergeix vers a un cert límit $u \in K$ satisfent $|f-u| = d = \inf_{v \in K} |f-v|$.
	
	Vegem ara la caracterització: començarem veient que si per a $u\in K$ es té $|f-u| = \min_{v \in K} \{ |f-v| \}$, 
	llavors se satisfan \ref{item:projconvi}, \ref{item:projconvii}. Suposem que $v \in K$, i $t \in [0,1]$. Llavors, 
	per convexitat,	$w = (1-t)u + tv \in K$. D'aquesta manera,
	\[
		|f-u| \leq |f-w| = |(f-u) - t(v-u)|.
	\]
	Aleshores:
	\[
		|f-u|^2 \leq |f-u|^2 -2t(f-u,v-u) + t^2 |v-u|^2,
	\]
	on hem usat la desigualtat de Cauchy-Schwarz així: $-2t(f-u,v-u) \geq -2|f-u||t(v-u)|$. D'aquesta manera,
	si $t \in (0,1]$, deduïm que $2(f-u,v-u) \leq t|v-u|^2$. Fent tendir $t \to 0$, deduïm que $(f-u,v-u) \leq 0$, com
	volíem.
	
	Recíprocament, suposem ara que $u$ satisfà \ref{item:projconvi}, \ref{item:projconvii}, i sigui $v \in K$. Es té:
	\begin{align*}
		|u-f|^2 - |v-f|^2 &= (u-f,u-f) - (v-f,v-f) \\
		& = (u-f,u-f) - (u-f,v-f) + (u-f,v-f) - (v-f,v-f) \\
		& = (u-f,u-v) + (u-v,v-f) \\
		& = (u-f,u-v) + (u-v,u-f) - (u-v,u-v) \\
		& = 2(f-u,v-u) - |u-v|^2  \leq 0,
	\end{align*}
	on a l'última desigualtat hem usat \ref{item:projconvii}. Llavors, s'ha de tenir $|f-u| \leq |f-v|$, com volíem.
	
	Seguidament, vegem que $u$ és única. Suposem que $u_1,u_2 \in K$ satisfan $ii)$, és a dir, $(f-u_i,v_i-u_i) \leq 0$
	per a $v_i \in K$, $i \in \{1,2\}$. En particular, escollint $v_1 = u_2$ i $v_2 = u_1$, queda:
	\[
		\begin{rcases}
			(f-u_1,u_2-u_1) \leq 0 \\
			(f-u_2,u_1-u_2) \leq 0 \\
		\end{rcases}
		\implies ((f-u_2) + (u_1-f),u_1-u_2) = |u_1-u_2|^2 \leq 0,
	\]
	de manera que $u_1 = u_2$.
	
	Finalment, si $u_1 = P_K f_1$ i $u_2 = P_K f_2$, vegem que $|u_2-u_1| \leq |f_2-f_1|$ (en particular, $P_K$ és contínua).
	Similarment a abans, tenim:
	\[
		\begin{rcases}
			(f_1-u_1,u_2-u_1) \leq 0 \\
			(f_2-u_2,u_1-u_2) \leq 0 \\
		\end{rcases}
		\implies |u_1-u_2|^2 +(f_2-f_1,u_1-u_2)\leq 0,
	\]
	Usant la desigualtat de Cauchy-Schwarz, deduïm que:
	\[
		|u_1-u_2|^2 \leq (f_1-f_2,u_1-u_2) \leq |f_1-f_2||u_1-u_2|,
	\]
	d'on deduïm la desigualtat demanada.
\end{proof}

\begin{teo}[Teorema de la projecció sobre un subespai tancat]
\label{teo:projsub}
	Sigui $H$ un espai de Hilbert, i $M \subset H$ un subespai vectorial tancat. Llavors, per a tot $f \in H$
	existeix un únic $u \in M$ tal que:
	\[
		|f-u| = \min_{v \in M} \{ |f-v| \}
	\]
	A més, $u$ es caracteritza per les dues propietats següents:
	\begin{enumerate}[i)]
		\item $u \in M$. \label{item:projsubi}
		\item $(f-u,v) = 0$, $\forall v \in M$. \label{item:projsubii}
	\end{enumerate}
	S'escriu $P_M f := u$, on $P_M$ és un operador lineal continu i una projecció que, a més, satisfà $\|P_M\| \leq 1$.
\end{teo}

\begin{proof}
	Només cal comprovar com ``s'actualitza'' la propietat \ref{item:projsubii}. Suposem primer que 
	$|f-u| = \min_{v \in M} \{ |f-v| \}$. Llavors, si $v \in M$, com que $u+v,u-v \in M$, pel teorema anterior se satisfà:
	\[
		\begin{rcases}
			(f-u,(u+v)-u) = (f-u,v) \leq 0 \\
			(f-u,(u-v)-u) = -(f-u,v) \leq 0 \\
		\end{rcases}
		\implies (f-u,v) = 0.
	\]
	D'altra banda, si se satisfan \ref{item:projsubi}, \ref{item:projsubii} (d'aquest teorema) i es té $v \in M$, 
	com que $v-u \in M$:
	\[
		(f-u,v-u) = 0 \leq 0,
	\]
	i ja hem acabat.
\end{proof}

A l'operador $P_M$ se li diu projecció ortogonal. Els dos teoremes anteriors s'interpreten de forma
més natural a través del concepte d'ortogonalitat en espais de Hilbert, que discutim tot seguit:

\begin{defi}[subespai ortogonal]
	Sigui $H$ un espai de Hilbert, i $M \subset H$ un subconjunt. Es defineix el subespai ortogonal a $M$ com:
	\[
		M^{\perp} = \left\{u \in H \, | \, (u,v) = 0, \, \forall v \in M \right\}.
	\]
\end{defi}

Notem que, malgrat la similitud amb el concepte anàleg en espais de Banach, les definicions són diferents
(en espais de Banach hem de recórrer a l'espai dual i el seu producte de dualitat).
Més endavant, amb el teorema de representació de Riesz-Fréchet (\ref{teo:riesz}),
veurem que els dos conceptes són equivalents.

\begin{prop}
	Si $M \subset H$ és un subespai vectorial tancat, aleshores $(M^{\perp})^{\perp} = M$.
\end{prop}

\begin{proof}
	Escrivim la definició:
	\[
		(M^{\perp})^{\perp} = \left\{x \in H \, | \, (x,y) = 0, \, \forall y \in M^{\perp} \right\}.
	\]
	És clar, llavors, que $M \subset (M^{\perp})^{\perp}$ (de fet, la inclusió és certa en general). Vegem 
	l'altra inclusió. Suposem que $f \in (M^{\perp})^{\perp}$. Llavors, pel teorema \ref{teo:projsub}, existeix
	$u \in M$ tal que $(f-u,v) = 0$, $\forall v \in M$; és a dir, $f-u \in M^{\perp}$. Usant les diferents ortogonalitats,
	observem que:
	\[
		0 = (f,f-u) = (f-u,f-u) + (u,f-u) = \|f-u\|^2,
	\]
	de manera que $f = u \in M$.
\end{proof}

\begin{ej}
	Demostreu que, més en general, si $S \subset H$ és un subconjunt, llavors:
	\[
		(S^{\perp})^{\perp} = \overline{\text{span}(S)}
	\]
\end{ej}
Tot seguit, presentem un lema purament d'àlgebra lineal:

\begin{lema}
	Sigui $H$ un espai vectorial, i considerem una aplicació lineal $P \colon H \to H$ que també sigui una
	projecció ($P^2 = P$). Aleshores:
	\begin{enumerate}[i)]
		\item $I-P$ també és projecció, i $\Im(I-P) = \ker(P)$.
		\item Tot element $f \in H$ s'expressa de forma única com $f = u_0 + v_0$, amb $u_0 \in \Im P$ i
		$v_0 \in \Im(I-P)$, i es té la fórmula $u_0 = Pf$, $v_0 = (I-P)f$.
	\end{enumerate} 
\end{lema}
\begin{proof}
	\begin{enumerate}[i)]
		\item[]
		\item Es té $(I-P)^2 = (I-P)(I-P) = I^2 - P - P + P^2 = I-P$. D'una banda:
		\[
			f \in \ker(P) \implies Pf = 0 \implies (I-P)f = f.
		\]
		De l'altra, si $h = (I-P)g = g-Pg$, es té:
		\[
			\begin{rcases}
				h-Ph = (I-P)(g-Pg) = g-Pg \implies &Pg -Ph = g-h \\
				&Pg = g-h
			\end{rcases}
			\implies Ph = 0.
		\]
		\item És clar que $f = Pf + (I-P)f$. Només cal veure que aquesta expressió és única. Suposem que
		$f = u_1 + v_1 = u_2 + v_2$, amb $u_i \in \Im(P)$, $v_i \in \Im(I-P)$, $i \in \{1,2\}$. Llavors,
		$u_1 - u_2 = v_2-v_1 \in \Im(P) \cap \ker(P)$. Per tant, per a cert $w \in H$:
		\[
			u_1 - u_2 = P(w) = P^2(w) = P(u_1-u_2) = 0.
		\]
	\end{enumerate}
\end{proof}

\begin{prop}
	Sigui $H$ de Hilbert i $M \subset H$ un subespai vectorial tancat. Aleshores:
	\begin{enumerate}[i)]
		\item $M^{\perp} = \ker P_{M}$.
		\item $I-P_M = P_{M^{\perp}}$.
		\item $H = M \oplus M^{\perp}$. En particular, $M$ admet un suplementari
            topològic, com havíem esmentat ja a la proposició
            \ref{prop:suplementaris}.
	\end{enumerate}
\end{prop}
\begin{proof}
	\begin{enumerate}[i)]
		\item[]
		\item D'una banda, sabem que per a tot $f \in H$, es té $f - Pf \in M^{\perp}$, és a dir,
		$M^{\perp} \supset \Im(I-P) = \ker(P)$. De l'altra, si $g \in M^{\perp}$, com que $Pg \in M$,
		se satisfà:
		\[
			0 = (g,Pg) = (g-Pg,Pg) +(Pg,Pg) = |Pg|^2.
		\]
		\item Si $f \in H$, per l'apartat anterior: $(I-P_M)f \in M^{\perp}$. A més, si $g \in M^{\perp}$:
		\[
			(f-(I-P_M)f,g) = (P_M f, g) = 0.
		\]
		Per la caracterització del teorema \ref{teo:projsub}, deduïm que $(I-P_M)f = P_{M^{\perp}} f$.
		\item Pel lema anterior, sabem que $H = \Im P_{M} \oplus \Im (I-P_{M})$. No obstant, com que $\Im P_M = M$
		i $\Im (I-P_M) = \Im P_{M^{\perp}} = M^{\perp}$, deduïm el resultat.
	\end{enumerate}
\end{proof}

En particular, se satisfà una versió del teorema de Pitàgores: si $f = u + v$, amb $u \in M$ i $v \in M^{\perp}$,
llavors:
\[
	|f|^2 = (u+v,u+v) = (u,u) + 2(u,v) + (v,v) = |u|^2 + |v|^2.
\]

\section{Teoremes de representació}
\begin{teo}[Teorema de representació de Riesz-Fréchet]
    \label{teo:riesz}
    Sigui $H$ un espai de Hilbert. Aleshores, per a tota $\varphi \in H'$
    existeix una única $f \in H$ tal que, per a tota $u \in H$,
    \begin{equation}
        \label{eq:riesz}
        \langle \varphi,u \rangle = (f,u).
    \end{equation}
    A més a més, $|f|_H = \|\varphi\|_{H'}$ i l'aplicació $j \colon \varphi \to
    f$ dóna una identificació isomètrica entre $H'$ i $H$.
\end{teo}
\begin{proof}
    Sigui $M = \varphi^{-1}(0)$, de manera que $M$ és un subespai vectorial
    tancat. Si $M = H$, aleshores prenent $f = 0$ la conclusió és immediata.
    En cas contrari, prenem $f_0 \in H$, $f_0 \not \in M$. Sigui $f_1 = P_M
    f_0$, i
    \[
        f = \frac{f_0-f_1}{|f_0-f_1|}.
    \]
    Aleshores $|f| = 1$ i per a tota $v \in M$
    \[
        (f, v) = \frac{(f_0-P_Mf_0, v)}{|f_0-f_1|} = 0.
    \]
    En particular, $f \not \in M$, d'on $\langle \varphi, f \rangle \neq 0$.
    Donada $u \in H$, definim
    \[
        v = u - \frac{\langle\varphi, u\rangle}{\langle \varphi, f \rangle}f,
    \]
    tenim $\langle \varphi, v \rangle = 0$, és a dir, $v \in M$ i per tant
    \[
        0 = (f, v) = (f, u) - \frac{\langle\varphi, u\rangle}
        {\langle \varphi, f \rangle} (f, f).
    \]
    Reordenant els termes, per a tota $u \in H$,
    \[
        \langle \varphi, u \rangle = \langle \varphi, f \rangle (f, u),
    \]
    d'on $f$ satisfà \eqref{eq:riesz}. Donada $g$ que també la satisfaci,
    aleshores
    \[
        |f-g| = (f-g, f-g) = (f, f-g) - (g, f-g) = \langle \varphi, f-g \rangle 
        - \langle \varphi, f-g \rangle = 0,
    \]
    que ens dóna la unicitat de $f$.
    Finalment,
    \[
        |\langle\varphi, v \rangle| = |(f, u)| \leq |f||u|,
    \]
    i la igualtat s'obté per $u = f$, d'on $\|\varphi\|_{H'} = |f|_H$.
\end{proof}
\begin{col}
    \begin{enumerate}[i)]
        \item[]
        \item $j$ indueix una estructura d'espai de Hilbert a $H'$.
        \item Tot espai de Hilbert és reflexiu.
    \end{enumerate}
\end{col}
\begin{proof}
    \begin{enumerate}[i)]
        \item []
        \item Per ser $j$ una isometria, podem dotar $H'$ d'un producte escalar
            mitjançant la identitat de polarització
            \[
                (\phi_1, \phi_2)_{H'} = \frac{1}{4} (\|\phi_1 + \phi_2\|_{H'}^2 - \|\phi_1
                - \phi_2\|_{H'}^2) = (j(\phi_1), j(\phi_2))_H.
            \]
        \item Podem identificar $H$ amb $H'$ mitjançant $j$ i anàlogament
            identificar $H'$ amb $H''$.
    \end{enumerate}
\end{proof}
\begin{defi}[forma bilineal contínua i coerciva]
    Diem que una forma bilineal $a \colon H \times H \to \real$ és
    \begin{enumerate}[i)]
        \item contínua si existeix una constant $C$ tal que, per a tota $u, v
            \in H$
            \[
                |a(u, v)| \leq C |u||v|;
            \]
        \item coerciva si existeix una constant $\alpha > 0$ tal que, per a tota
            $v \in H$,
            \[
                \alpha(v, v) \geq \alpha|v|^2.
            \]
    \end{enumerate}
\end{defi}
\begin{teo}[Teorema de Lax-Milgram]
    \label{teo:L-M}
    Sigui $H$ un espai de Hilbert i $a\colon H \times H \to \real$ una forma
    bilineal contínua i coerciva (no necessàriament simètrica). Per a tota
    $\varphi \in H'$, existeix una única $u \in H$ tal que per a tota $v \in H$
    \begin{equation}
        \label{eq:laxmil}
        \langle \varphi, v \rangle = a(u, v).
    \end{equation}
    A més, si $a$ és simètrica, $u$ es caracteritza per:
    \[
        \frac{1}{2}a(u, u) - \langle \varphi, u \rangle = \min \left\{
            \frac{1}{2}a(v, v) - \langle \varphi, v \rangle \mid v \in H \right\}.
    \]
\end{teo}
\begin{proof}
    Fixada $u \in H$, l'aplicació $v \mapsto a(u, v)$ és lineal i contínua,
    d'on usant Riesz-Fréchet (\ref{teo:riesz}), existeix un únic element a $H$,
    que denotarem per $Au$, tal que $a(u, v) = (Au, v)$ per a tota $v \in H$.
    És senzill veure que $A$ és un operador lineal de $H$ a $H$. A més, per a
    tota $u \in H$, es té
    \[
        |Au|^2 = (Au, Au) = a(u, Au) \leq C|u||Au|,
    \]
    d'on, dividint per $|Au|$ si $Au \neq 0$, obtenim,
    \[
        |Au| \leq C|u|.
    \]
    És a dir, $|A|$ és contínua. A més,
    \begin{equation}
        \label{eq:laxmilcoerc}
        (Au, u) = a(u, u) \geq \alpha |u|^2.
    \end{equation}
    
    Sigui $f \in H$ la identificació de $\varphi$ a $H$ altra vegada per
    Riesz-Fréchet. Demostrar que \eqref{eq:laxmil} té una única solució és
    equivalent a demostrar que per a tota $f \in H$ existeix una única $u \in
    H$ tal que
    \[
        (f, v) = (Au, v) \quad \forall v \in H
    \]
    és a dir, que $Au = f$. Això és precisament demostrar que $A$ és bijectiva.
    Ara bé, \eqref{eq:laxmilcoerc} ens assegura que $A$ és injectiva. Per
    a veure que és exhaustiva, veurem que $R(A)$ és tancat i dens.
    Usant Cauchy-Schwarz també a \eqref{eq:laxmilcoerc} tenim
    \[
        \alpha|v| \leq |Av| \quad \forall v \in H,
    \]
    de manera que per a tota successió de Cauchy $v_n = Au_n$ a $R(A)$,
    \[
        |u_n-u_m| \leq \frac{|Au_n - Au_m|}{\alpha} = \frac{|v_n-v_m|}{\alpha}.
    \]
    Per tant, $\lim v_n = \lim A u_n = A \lim u_n \in R(A)$, és a dir, $R(A)$
    és tancat. Finalment, si $v \in H$ satisfà $(Au, v) = 0$ per a tota
    $u \in H$, aleshores,
    \[
        a(u, v) = (Au, v) = 0 \quad \forall u \in H,
    \]
    d'on $v = 0$, i fent servir el teorema \ref{item:iteoperp}, $\overline{R(A)}
    = (R(A)^{\perp})^{\perp} = \left\{ 0 \right\}^{\perp} = H$.

    Suposem ara que $a$ és simètrica. Aleshores $a(\cdot, \cdot)$ ens dóna un
    nou producte escalar sobre $H$. La continuïtat i la coercivitat de $a$ ens
    asseguren que la norma $a(\cdot, \cdot)^{\frac{1}{2}}$ és equivalent a la
    norma que ja teníem, per tant $H$ també és de Hilbert amb la norma induïda
    per $a$. Donada $\varphi \in H'$, si fem servir altra vegada Riesz-Fréchet
    però aquest cop amb el nou producte escalar $a(\cdot, \cdot)$, existeix un
    únic $u \in H$ tal que satisfà \eqref{eq:laxmil}. Aleshores de
    $a(u-v, u-v) \geq 0$ obtenim, desenvolupant els termes,
    \[
        -\frac{1}{2} a(u, u) \leq \frac{1}{2} a(v, v) - a(u, v) \quad \forall v \in H,
    \]
    és a dir,
    \[
        \frac{1}{2}a(u, u) - \langle \varphi, u \rangle \leq
        \frac{1}{2} a(v, v) - \langle \varphi, v \rangle \quad \forall v \in H,
    \]
    i la igualtat es dóna només si $v = u$.

\end{proof}

\section{Suma Hilbertiana. Base Hilbertiana}

\begin{defi}[suma Hilbertiana]
	Sigui $(E_n)$ una successió de subespais tancats d'un espai de Hilbert $H$. Direm que $H$
	és la suma Hilbertiana dels $E_n$, i escriurem $H = \bigoplus_n E_n$, si:
	\begin{itemize}
		\item Els espais són mútuament ortogonals, és a dir, si $u \in E_n$, $v \in E_m$ i $n \neq m$,
		llavors $(u,v) = 0$.
		\item El subespai vectorial generat per $\cup_n E_n$ és dens en $H$.
	\end{itemize}
\end{defi}

\begin{teo*}
	\label{teo:sumaHilbert}
	Suposem que $H$ és suma Hilbertiana de $E_n$. Sigui $u \in H$, i sigui $u_n = P_{E_n} u$. Considerem
	$S_n = \sum_{k=1}^n u_k$. Llavors:
	\begin{enumerate}[i)]
		\item $\lim_{n \to \infty} S_n = u$.
		\item $|u|^2 = \sum |u_k|^2$ (Identitat de Bessel-Parseval).
	\end{enumerate}
\end{teo*}

Per a demostrar el teorema usarem un lema, que es pot interpretar com una mena de recíproc:

\begin{lema}
	\label{lema:sumaHilbert}
	Considerem una successió $(u_n)$ de $H$ tal que $(u_n,u_m) = 0$ si $n \neq m$ i
	$\sum |u_k|^2 < \infty$. Si $S_n = \sum_{k = 1}^n u_k$, llavors:
	\begin{enumerate}[i)]
		\item Existeix $S = \lim_{n \to \infty} S_n$.
		\item $|S|^2 = \sum |u_k|^2$.
	\end{enumerate}
\end{lema}
\begin{proof}
	Per a demostrar que $S_n$ és convergent, veurem que és de Cauchy. Com que $\sum |u_k|^2 < \infty$
	per a $n,m$ suficientment grans, $n > m$:
	\[
		|S_n-S_m|^2 = (S_n - S_m, S_n-S_m) = \sum_{k = m+1}^n |u_k|^2 < \varepsilon.
	\]
	Per tant, existeix $S = \lim_{n \to \infty} S_n$. El segon apartat es dedueix de:
	\[
		|S_n|^2 = \sum_{k = 1}^n |u_k|^2,
	\]
	i fent $n \to \infty$.
\end{proof}

\begin{proof}[del teorema \ref{teo:sumaHilbert}]
	Utilitzarem el lema per veure que la successió $S_n = \sum_{k = 1}^n u_k$ és convergent.
	D'una banda, per a tot $k$ es pot escriure $u = u_k + v$, on $v \in E_k^{\perp}$. Llavors,
	es té $(u,u_k) = |u_k|^2$. Si sumem per $k$, obtenim:
	\[
		(u,S_n) = \sum_{k = 1}^n |u_k|^2 = |S_n|^2.
	\]
	D'aquí es dedueix que:
	\[	
		(S_n,S_n) = (u,S_n) \implies (u-S_n,S_n) = 0.
	\]
	D'aquesta manera, concloem que:
	\[
		|u|^2 = (u,S_n) + (u,u-S_n) = |S_n|^2 + (u-S_n,u-S_n) + (S_n,u-S_n) \geq |S_n|^2.
	\]
	En resum, es té la fita $|S_n| \leq |u|$, i per tant $S_n$ tendeix a un cert límit $S$. Tot seguit,
	identifiquem quin és el límit $S$. Observem que si $v \in E_m$, per a cert $m \leq n$:
	\[
		(u-S_n, v) = (u-u_m,v) - \sum_{k \neq m} (u_k,v) = 0,
	\]
	on el primer terme és zero pel teorema \ref{teo:projsub} i el segon terme és zero per ortogonalitat.
	Així, fent $n \to \infty$:
	\[
		(u-S,v) = 0, \; \forall v \in E_m \implies (u-S,v) = 0, \; \forall v \in F, 
	\]
	on $F$ és el subespai vectorial generat pels $E_n$. Això implica que
	\[
		(u-S,v) = 0, \; \forall v \in \overline{F} = H. 
	\]
	En particular, posant $v = u-S$, concloem que $S = u$.
\end{proof}

\begin{defi}[base Hilbertiana]
	Una successió $(e_n)$ en $H$ és una base Hilbertiana si satisfà les següents propietats:
	\begin{enumerate}[i)]
		\item Ortonormalitat: $(e_n,e_m) = \delta_{nm}$.
		\item El subespai vectorial generat pels $e_n$ és dens en $H$.
	\end{enumerate}
\end{defi}

\begin{teo*}
	Tot espai de Hilbert separable (i.e. que conté un conjunt numerable dens) admet una base
	Hilbertiana.
\end{teo*}
\begin{proof}
	Sigui $(v_n)$ un conjunt numerable dens de l'espai de Hilbert $H$. Denotem per $F_k$
	el subespai vectorial generat per $\{v_1, \dots , v_k \}$. Per a construir la base ortonormal,
	prenem $e_1$ de $F_1$, tal que tingui norma 1. Llavors, si $F_2 \neq F_1$, llavors existeix un
	vector $e_2$ de norma 1 i ortogonal a $e_1$ tal que $\{e_1,e_2 \}$ genera $F_2$. Inductivament,
	obtenim una successió $(e_n)$.
	
	Observem que $\cup_{k = 1}^n F_k$ és dens en $H$, d'on concloem que $(e_n)$ és base Hilbertiana.
\end{proof}

\begin{col}
	Tot espai de Hilbert separable és isomorf i isomètric amb l'espai $\ell^2$.
\end{col}
\begin{proof}
	Sigui $(e_n)$ una base ortonormal de l'espai de Hilbert $H$. Observem que pel teorema 
	\ref{teo:sumaHilbert}, tota $u \in H$ es pot escriure de la forma
	\[
		u = \sum_{n = 1}^{\infty} (u,e_n) e_n, 
	\]
	amb 
	\[
		|u|^2 = \sum_{n = 1}^{\infty} |(u,e_n)|^2,
	\]
	ja que $E_n = \langle e_n \rangle$ és suma Hilbertiana de $H$ i $P_{E_n} u = (u,e_n) e_n$. A la 
	vista d'aquest resultat, definim l'aplicació $T \colon H \to \ell^2$, definida per:
	\[
		T(u) = \left( (u,e_n) \right)_{n \geq 1}.
	\]
	L'aplicació està ben definida i és lineal. A més, donada una successió $(\alpha_n) \in \ell^2$,
	pel lema \ref{lema:sumaHilbert}, tindrem que la sèrie
	\[
		\sum_{n \geq 1} \alpha_n e_n
	\]
	convergeix a un cert $u \in H$, tal que $T(u) = (\alpha_n)$. Per tant, com que $T$ és invertible,
	és bijectiva, i per tant un isomorfisme. A més, és una isometria:
	\[
		|u|^2 = \sum |(u,e_n)|^2 = |T(u)|_{\ell^2}^2.
	\]
\end{proof}

